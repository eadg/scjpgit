/** Created on 2009-03-23 */
package chapter_10;

import java.util.*; //F4

public class F4_I {
	public static void main(String[] args) {
		Properties p = System.getProperties();
		p.setProperty("myProp", "myValue");
		p.list(System.out);
		System.out.println(p.getProperty("myProp"));
	}
}
