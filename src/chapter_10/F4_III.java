package chapter_10;

//import static java.lang.System.out;       // 1
import static java.lang.System.*; // 1
import static java.lang.Integer.*; // 2

public class F4_III {
	public static void main(String[] args) {
		out.println(MAX_VALUE); // 3
		out.println(toHexString(42)); // 4
		gc();
	}
}
