package chapter_10.Q6.xcom;

public class Stuff {
	public static final int MY_CONSTANT = 5;

	public static int doStuff(int x) {
		return (x++) * x;
	}
}
