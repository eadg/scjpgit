package chapter_10.Q6;

import static chapter_10.Q6.xcom.Stuff.*;
import static java.lang.System.out;

class User {
	public static void main(String[] args) {
		new User().go();
	}

	void go() {
		out.println(doStuff(MY_CONSTANT));
	}
}
