package getSCJP;

public class TestClass {
	static String sharedVar;

	public static void main(String[] args) {
		Thread setThread = new Thread() {
			public void run() {
				sharedVar = "jaka� warto��";
			}
		};
		// uruchamiamy w�tek przypisuj�cy warto�� do zmiennej sharedVar
		setThread.start();

		try {
			setThread.join(1); // czekamy az w�tek przypisuj�cy warto�� si�
								// zako�czy
		} catch (InterruptedException exc) {
		}
		// wy�wietlamy warto�� ustawion� przez w�tek na kt�ry czekali�my

		System.out.println(sharedVar);
	}

}
