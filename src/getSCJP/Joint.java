package getSCJP;

public class Joint {

	static String sharedVar;

	public static void main(String[] args) {
		Thread setThread = new Thread() {
			public void run() {
				sharedVar = "jakaś wartość";
			}
		};
		// uruchamiamy wątek przypisujący wartość do zmiennej sharedVar
		setThread.start();
		try {
			setThread.join(); // czekamy aŜ wątek przypisujący wartość się
			System.out.println("czekamy");
			// zakończy
		} catch (InterruptedException exc) {
		}
		// wyświetlamy wartość ustawioną przez wątek na który czekaliśmy
		System.out.println(sharedVar);
	}
}