package getSCJP;

class DataBuffer {
	private Integer numericData;

	public synchronized void setData(int value) {
		numericData = value;
		notifyAll(); // wzbudź wątki oczekujące na wypełnienie bufora
	}

	public synchronized int getData() {
		while (numericData == null) {
			try {
				wait(); // poczekaj, aŜ inny wątek wypełni bufor
			} catch (InterruptedException exc) {
				// ignorujemy ten wyjątek - jego wystąpienie nic nie szkodzi
			}
		}
		int value = numericData;
		numericData = null; // po pobraniu danych wyczyść bufor
		return value;
	}
}
