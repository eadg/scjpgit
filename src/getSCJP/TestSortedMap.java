package getSCJP;

import java.util.*;

public class TestSortedMap {
	public void addTest() {
		SortedMap<String, Object> map = new TreeMap<String, Object>();
		map.put("A", new Object());
		map.put("D", new Object());
		map.put("G", new Object());
		System.out.println(map.containsKey("B"));
		SortedMap<String, Object> subMap = map.headMap("D");
		subMap.put("B", new Object());
		System.out.println(map.containsKey("B"));
		System.out.println(map);
		System.out.println(subMap);
		System.out.println(map.firstKey());
		System.out.println(map.lastKey());
		Set ss = map.entrySet();
		System.out.println("ss: " + ss);
	}

	public static void main(String[] args) {
		TestSortedMap tsm = new TestSortedMap();
		tsm.addTest();
	}

}
