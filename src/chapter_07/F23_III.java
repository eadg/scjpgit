package chapter_07;

import java.util.*; //F23

class F23_III {
	public static void main(String[] args) {
		boolean[] ba = new boolean[5];
		Set<Object> s = new HashSet<Object>(); // insert this code
		ba[0] = s.add("a");
		ba[1] = s.add(new Integer(42));
		ba[2] = s.add("b");
		ba[3] = s.add("a");
		ba[4] = s.add(new Object());

		for (int x = 0; x < ba.length; x++)
			System.out.print(ba[x] + " ");
		System.out.println("\n");
		for (Object o : s)
			System.out.print(o + " ");
	}
}
