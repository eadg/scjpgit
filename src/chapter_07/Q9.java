package chapter_07;

import java.util.*;

public class Q9 {
	public static void main(String[] args) {
		String[] tab = new String[5];
		tab[0] = "A";
		tab[1] = "B";
		tab[2] = "C";
		tab[3] = " ";
		tab[4] = "1";
		Arrays.sort(tab);
		int i = Arrays.binarySearch(tab, " ");
		System.out.println(i);
	}
}