/** Created on 2008-12-07 */
package chapter_07;

import java.util.*;

public class F35_IV { // F35
	public static void main(String[] args) {
		List<Integer> myList = new ArrayList<Integer>();
		Bar_F35 bar = new Bar_F35();
		// bar.doInsert(myList);
	}
}

class Bar_F35 {
	// void doInsert(List<?> list) {
	void doInsert(List<Object> list) { // doInsert(List<?> list) is ok
		list.add(new Dog_F24());
		// list.addAll(new Dog_F24());
	}
}
