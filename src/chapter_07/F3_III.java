/** Created on 2008-11-20 */
package chapter_07;

import java.util.*; //F2

public class F3_III {
	public static void main(String[] args) {
		List<String> test = new ArrayList<String>();
		String s = "hi";
		test.add("string");
		test.add(s);
		test.add(s + s);
		System.out.println(test.size());
		System.out.println(test.contains(42));
		System.out.println(test.contains("hihi"));
		test.remove("hi");
		System.out.println(test.size());
		System.out.println(test.get(1));
	}
} // 3 \n false \n true \n 2
