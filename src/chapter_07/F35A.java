/** Created on 2008-12-14 */
package chapter_07;

import java.util.*;

public class F35A {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<?> list = new ArrayList<Dog>();
		List<? extends Animal> aList = new ArrayList<Dog>();
		// List<?> foo = new ArrayList<? extends Animal>();
		// List<? extends Dog> cList = new ArrayList<Integer>();
		List<? super Dog> bList = new ArrayList<Animal>();
		// List<? super Animal> dList1 = new ArrayList<Dog>();
		// my Examples:
		List<? super Animal> dList2 = new ArrayList<Animal>();
		List<? super Animal> dList3 = new ArrayList<Object>();
	}

}
