/** Created on 2008-12-13 */
package chapter_07;

import java.util.*; //F41

public class F41_IV {
	public <T> void makeArrayList(T t) {
		// take object of unknown type and use "T" to represent type
		List<T> list = new ArrayList<T>();
		// now we can create the list using "T"
		list.add(t);
		System.out.println(list.size());
	}

	public static void main(String[] args) {
		F41_IV f41 = new F41_IV();
		f41.makeArrayList("string");
	}
}
