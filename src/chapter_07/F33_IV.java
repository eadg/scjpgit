package chapter_07;

import java.util.ArrayList;
import java.util.List;

public class F33_IV {

	public void addAnimal(List<? super Dog> animals) {
		animals.add(new Dog());
	} // add is sometimes OK with super

	public static void main(String[] args) { // F33
		List<Animal> animals = new ArrayList<Animal>();
		animals.add(new Dog());
		animals.add(new Dog());
		// AnimalDoctorGeneric doc = new AnimalDoctorGeneric();
		// doc.addAnimal(animals);
	} // passing Animal List

}
