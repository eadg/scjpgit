/** Created on 2008-12-14 */
package chapter_07;

import java.util.*;

public class Q5 {
	public static void main(String[] args) {
		Queue<String> q = new LinkedList<String>();
		q.add("Veronica");
		q.add("Wallace");
		q.add("Duncan");
		showAll(q);
	}

	public static void showAll(Queue q) {
		// public static void showAll(Queue<String> q) {
		q.add(new Integer(42));
		while (!q.isEmpty())
			System.out.print(q.remove() + "  ");
	}
}
