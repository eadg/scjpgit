/** Created on 2008-12-04 */
package chapter_07;

import java.util.*; //F10

public class F10_IV {
	public static void main(String[] args) {
		List<Integer> myList = new ArrayList<Integer>();
		myList.add(4);
		myList.add(6);
		myList.add(new Integer(5));
		Inserter in = new Inserter();
		in.insert(myList); // pass List<Integer> legacy code
		System.out.print(myList.toString());
	}
}

class Inserter {
	// method with non-generic List argument
	// void insert(List<Integer> list) { //works!
	void insert(List list) {
		list.add(new Integer(42));
		list.add(new String("42"));
	}
}
