/** Created on 2008-12-14 */
package chapter_07;

//Nie przerobiony... it's sick!
interface Hungry<E> {
	void munch(E x);
}

interface Carnivore<E extends Animal_Q10> extends Hungry<E> {
}

interface Herbivore<E extends Animal_Q10> extends Hungry<E> {
}

abstract class Plant {
}

class Grass extends Plant {
}

abstract class Animal_Q10 {
}

class Sheep extends Animal_Q10 implements Herbivore<Sheep> {
	public void munch(Sheep x) {
	}
}

class Wolf extends Animal_Q10 implements Carnivore<Sheep> {
	public void munch(Sheep x) {
	}
}

public class Q10 {

}
