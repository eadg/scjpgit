/** Created on 2008-12-06 */
package chapter_07;

import java.util.*;

abstract class Animal_F24 {
	public abstract void checkup();

	public void addAnimal(ArrayList<Animal> animals) {
		animals.add(new Dog());
	}
}

class Dog_F24 extends Animal {
	public void checkup() {
		System.out.println("Dog_F24check");
	}
}

class Cat_F24 extends Animal {
	public void checkup() {
		System.out.println("Cat_F24check");
	}
}

class Bird_F24 extends Animal {
	public void checkup() {
		System.out.println("Bird_F24check");
	}
}

public class F24_IV { // F24
	public void checkAnimals(ArrayList<Animal> animals) {
		for (Animal a : animals) {
			a.checkup();
		}
	}

	public static void main(String[] args) {
		// make ArrayLists instead of arrays for Dog_F24,Cat_F24,Bird_F24
		List<Dog_F24> Dog_F24s = new ArrayList<Dog_F24>();
		Dog_F24s.add(new Dog_F24());
		Dog_F24s.add(new Dog_F24());
		List<Cat_F24> Cat_F24s = new ArrayList<Cat_F24>();
		Cat_F24s.add(new Cat_F24());
		Cat_F24s.add(new Cat_F24());
		List<Bird_F24> Bird_F24s = new ArrayList<Bird_F24>();
		Bird_F24s.add(new Bird_F24());
		// this code is same as Array version
		F24_IV doc = new F24_IV();
		// it worked when we used arrays instead of ArrayLists:
		// doc.checkAnimals(Dog_F24s); // send List<Dog_F24>
		// doc.checkAnimals(Cat_F24s); // send List<Cat_F24>
		// doc.checkAnimals(Bird_F24s); // send List<Bird_F24>
	}
}
