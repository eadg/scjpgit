/** Created on 2008-11-04 */
package chapter_07;

import java.io.*;

class SaveMe implements Serializable {
	transient int x;

	int y; // F11

	SaveMe(int xVal, int yVal) {
		x = xVal;
		y = yVal;
	}

	public int hashCode() {
		return (x ^ y);
		// Legal, but not correct to use transient variable
	}

	public boolean equals(Object o) {
		SaveMe test = (SaveMe) o;
		if (test.y == y && test.x == x) {
			// Legal, not correct
			return true;
		} else {
			return false;
		}
	}
}
