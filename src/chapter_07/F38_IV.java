/** Created on 2008-12-13 */
package chapter_07;

import java.util.*;

class F38_IV { // F38
	public static void main(String[] args) {
		// make some Cars for pool
		Car c1 = new Car();
		Car c2 = new Car();
		List<Car> carList = new ArrayList<Car>();
		carList.add(c1);
		carList.add(c2);
		F37_IV<Car> carRental = new F37_IV<Car>(5, carList);
		// now get car out, and it won't need cast
		Car carToRent = carRental.getRental();
		carRental.returnRental(carToRent);
		carRental.returnRental(new Car());
		// carRental.returnRental(new String());
		// can we stick something else in original carList?
		// carList.add(new Bike());
		for (Car c : carList) {
			System.out.println(c);
		}
	}
}
