/** Created on 2008-12-13 */
package chapter_07;

import java.util.*; //F37

public class F37_IV<T> { // "T" is for type parameter
	private List<T> rentalPool; // Use class type for List type

	private int maxNum;

	public F37_IV(int maxNum, List<T> rentalPool) { // constructor takes List of
													// class type
		this.maxNum = maxNum;
		this.rentalPool = rentalPool;
	}

	public T getRental() { // we rent out T blocks until there's smt available
		return rentalPool.get(0);
	}

	public void returnRental(T returnedThing) { // and renter returns T
		rentalPool.add(returnedThing);
	}

}
