/** Created on 2008-11-27 */
package chapter_07;

import java.util.*; //F22

class F22_III_Dog {
	public String name;

	F22_III_Dog(String n) {
		name = n;
	}
}

class F22_III {
	public static void main(String[] args) {
		List<F22_III_Dog> d = new ArrayList<F22_III_Dog>();
		F22_III_Dog dog = new F22_III_Dog("aiko");
		d.add(dog);
		d.add(new F22_III_Dog("clover"));
		d.add(new F22_III_Dog("magnolia"));
		Iterator<F22_III_Dog> i3 = d.iterator(); // make iterator
		while (i3.hasNext()) {
			F22_III_Dog d2 = i3.next(); // cast not required
			System.out.println(d2.name);
		}
		for (F22_III_Dog fdog : d) {
			System.out.println(fdog.name);
		}
		System.out.println("size " + d.size());
		System.out.println("get(1) " + d.get(1).name);
		System.out.println("aiko_index " + d.indexOf(dog));
		d.remove(2);
		Object[] oa = d.toArray();
		for (Object o : oa) {
			F22_III_Dog d2 = (F22_III_Dog) o;
			System.out.println("oa " + d2.name);
		}
	}
}
