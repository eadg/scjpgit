/** Created on 2008-12-13 */
package chapter_07;

public class F40_IV<T, X> { // F40A
	T one;

	X two;

	F40_IV(T one, X two) {
		this.one = one;
		this.two = two;
	}

	T getT() {
		return one;
	}

	X getX() {
		return two;
	}

	// test it by creating it with <String, Integer>

	public static void main(String[] args) {
		F40_IV<String, Integer> twos = new F40_IV<String, Integer>("foo", 42);
		String theT = twos.getT(); // returns String
		int theX = twos.getX(); // returns Integer, unboxes to int
	}
}
