/** Created on 2008-12-02 */
package chapter_07;

import java.util.*;

class CompMe implements Comparator {
	public int compare(Object o1, Object o2) {
		return ((String) o1).compareTo((String) o2);
	}
}

public class SortArray_my {
	public static void main(String[] args) {
		char[] c = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
				'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'w', 'x', 'y', 'z' };
		String[] s = new String[50];
		Random r = new Random();
		for (int i = 0; i < s.length; i++)
			s[i] = "" + c[r.nextInt(22)] + c[r.nextInt(22)] + c[r.nextInt(22)];
		/*
		 * for (String sa : s) System.out.println(sa);
		 */
		Arrays.sort(s, new CompMe());
		// Arrays.sort(s);
		for (String sa : s)
			System.out.println(sa);
	}
}
