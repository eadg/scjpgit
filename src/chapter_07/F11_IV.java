/** Created on 2008-12-04 */
package chapter_07;

import java.util.*; //F10

public class F11_IV {
	public static void main(String[] args) {
		List<Integer> myList = new ArrayList<Integer>();
		myList.add(4);
		myList.add(6);
		myList.add(new Integer(5));
		Inserter_F11 in = new Inserter_F11();
		in.insert(myList); // pass List<Integer> legacy code
		System.out.print(myList.toString());
	}
}

class Inserter_F11 {
	// method with non-generic List argument
	// void insert(List<Integer> list) { //not works!!
	void insert(List list) { // F11
		list.add(new String("42")); // put String in list passed in
	}
}
