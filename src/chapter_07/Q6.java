/** Created on 2008-12-14 */
package chapter_07;

import java.util.*;

public class Q6 {
	public static void before() {
		Set set = new TreeSet();
		set.add("2");
		set.add(3);
		set.add("1");
		Iterator it = set.iterator();
		while (it.hasNext())
			System.out.print(it.next() + " ");
	}

	public static void main(String[] args) {
		Q6 qtest = new Q6();
		qtest.before();
	}
}
