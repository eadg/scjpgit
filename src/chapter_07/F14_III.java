package chapter_07;

import java.util.*; //F14

class F14_III implements Comparator<F13_III> {

	public int compare(F13_III one, F13_III two) {

		return one.getTitle().compareTo(two.getTitle());
	}
}
