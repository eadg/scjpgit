/** Created on 2008-12-13 */
package chapter_07;

public class F39_IV<T> { // as class type
	T anInstance; // as instance variable type

	T[] anArrayOfTs; // as array type //F39

	F39_IV(T anInstance) { // as argument type
		this.anInstance = anInstance;
	}

	T getT() {
		return anInstance;
	}
}
