/** Created on 2008-12-06 */
package chapter_07;

import java.util.*;

abstract class Animal_F29 {
	public abstract void checkup();
}

class Dog_F29 extends Animal_F29 {
	public void checkup() {
		System.out.println("Dog_F29check");
	}
}

class Cat_F29 extends Animal_F29 {
	public void checkup() {
		System.out.println("Cat_F29check");
	}
}

class Bird_F29 extends Animal_F29 {
	public void checkup() {
		System.out.println("Bird_F29check");
	}
}

public class F29_IV { // F29

	public void foo() { // F28
		Dog_F29[] dogs = { new Dog_F29(), new Dog_F29() };
		addAnimal(dogs); // no problem
		printAnimal(dogs);
	}

	public void addAnimal(Animal_F29[] animals) {
		animals[0] = new Cat_F29();
	}

	public void printAnimal(Animal_F29[] animals) {
		for (Animal_F29 anim : animals) {
			anim.checkup();
		}
	}

	public static void main(String[] args) {
		F29_IV testAnimal = new F29_IV();
		testAnimal.foo();

	}
}
