/** Created on 2008-12-04 */
package chapter_07;

import java.util.*;

class Adder { // F9
	int addAll(List list) {
		// method with non-generic List argument, but assumes (with no
		// guarantee) that it will be Integers
		Iterator it = list.iterator();
		int total = 0;
		while (it.hasNext()) {
			int i = ((Integer) it.next()).intValue();
			total += i;
		}
		return total;
	}
}

public class F9_IV {
	public static void main(String[] args) {
		List<Integer> myList = new ArrayList<Integer>();
		myList.add(4);
		myList.add(6);
		Adder adder = new Adder();
		int total = adder.addAll(myList);
		// pass it to untyped argument
		System.out.println(total);
	}
}
