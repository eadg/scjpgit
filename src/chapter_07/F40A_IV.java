/** Created on 2008-12-13 */
package chapter_07;

import java.util.*; //F40A

public class F40A_IV<T extends Animal> {
	// use "T" instead of "?"
	T animals;

	public static void main(String[] args) {
		F40A_IV<Dog> dogHolder = new F40A_IV<Dog>(); // OK
		// F40A<Integer> x = new F40A<Integer>(); // NO!
	}
}