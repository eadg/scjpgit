/** Created on 2008-12-14 */
package chapter_07;

import java.util.*;

class Q7 {
	public static void main(String[] args) {
		Map<ToDos, String> m = new HashMap<ToDos, String>();
		ToDos tl = new ToDos("Monday");
		ToDos t2 = new ToDos("Monday");
		ToDos t3 = new ToDos("Tuesday");
		m.put(tl, "doLaundry");
		m.put(t2, "payBills");
		m.put(t3, "cleanAttic");
		System.out.println(m.size());
	}
}

class ToDos {
	String day;

	ToDos(String d) {
		day = d;
	}

	public boolean equals(Object o) {
		return ((ToDos) o).day == this.day;
	}
	// public int hashCode() { return 9; }
}
