/** Created on 2008-11-22 */
package chapter_07;

class F13_III implements Comparable<F13_III> {
	String title;

	String genre;

	String leadActor;

	F13_III(String t, String g, String a) {
		title = t;
		genre = g;
		leadActor = a;
	}

	public String toString() {
		return title + " " + genre + " " + leadActor + "\n";
	}

	public int compareTo(F13_III d) {
		return title.compareTo(d.getTitle());
	}

	public String getTitle() {
		return title;
	}

}
