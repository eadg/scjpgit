/** Created on 2008-12-07 */
package chapter_07;

import java.util.*; //F23

abstract class Animal_F23 {
	public abstract void checkup();
}

class Dog_F23 extends Animal_F23 {
	public void checkup() {
		System.out.println("Dog_F23check");
	}
}

class Cat_F23 extends Animal_F23 {
	public void checkup() {
		System.out.println("Cat_F23check");
	}
}

class Bird_F23 extends Animal_F23 {
	public void checkup() {
		System.out.println("Birdcheck");
	}
}

public class F23_IV {
	public void checkAnimals(Animal_F23[] animals) {
		for (Animal_F23 a : animals) {
			a.checkup();
		}
	}

	public static void main(String[] args) {
		Dog_F23[] dogs = { new Dog_F23(), new Dog_F23() };
		Cat_F23[] cats = { new Cat_F23(), new Cat_F23(), new Cat_F23() };
		Bird_F23[] birds = { new Bird_F23() };
		F23_IV doc = new F23_IV();
		doc.checkAnimals(dogs);
		doc.checkAnimals(cats);
		doc.checkAnimals(birds);
	}
} // OK

