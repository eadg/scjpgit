/** Created on 2008-11-27 */
package chapter_07;

import java.util.*; //F25

class Dog_F25b {
	public Dog_F25b(String n) {
		name = n;
	}

	public String name;

	public boolean equals(Object o) {
		if ((o instanceof Dog_F25b) && (((Dog_F25b) o).name == name)) {
			// Metoda nie jest jawnie uruchamiana...out jejst poza System.out.
			System.out.println("equals.." + ((Dog_F25b) o).name);
			System.out.println(name);
			return true;
		} else {
			return false;
		}
	}

	public int hashCode() {
		return name.length();
	}

	public String toString() {
		return name;
	}
}

class Cat_F25b {
	public Cat_F25b(String s) {
		cname = s;
	}

	public String cname;

	public int hashCode() {
		return cname.length();
	}

	public boolean equals(Object o) {
		if ((o instanceof Cat_F25b) && (((Cat_F25b) o).cname == cname)) {
			// Metoda nie jest jawnie uruchamiana...out jejst poza System.out.
			System.out.println("equals.." + ((Cat_F25b) o).cname);
			System.out.println(cname);
			return true;
		} else {
			return false;
		}
	}
}

enum Petsb {
	Dog_F25b, CAT_F25, HORSE
}

class F25b_III {
	public static void main(String[] args) {
		Map<Object, Object> m = new HashMap<Object, Object>();
		m.put("k1", new Dog_F25b("aiko")); // add some key/value pairs
		m.put("k2", Petsb.Dog_F25b);
		m.put(Petsb.CAT_F25, "CAT_F25 key");
		Dog_F25b d1 = new Dog_F25b("clover"); // keep this reference
		m.put(d1, "Dog_F25b key");
		Cat_F25b kot = new Cat_F25b("klot");
		m.put(kot, "Cat key");
		// String k1 = "k1";
		System.out.println("#1 " + m.get("k1")); // #1
		String k2 = "k2";
		System.out.println("#2 " + m.get(k2)); // #2
		Petsb p = Petsb.CAT_F25;
		System.out.println("#3 " + m.get(p)); // #3
		System.out.println("#4 " + m.get(d1)); // #4
		System.out.println("#5 " + m.get(new Cat_F25b("klot"))); // #5
		System.out.println("#5a " + m.get(kot)); // #5
		System.out.println("#6 " + m.size()); // #6
		d1.name = "magnolia";
		System.out.println("#7 " + m.get(d1)); // 7
		d1.name = "clover";
		System.out.println("#8 " + m.get(d1)); // 8
		d1.name = "clover";
		System.out.println("#9 " + m.get(new Dog_F25b("clover")));

	}
}
