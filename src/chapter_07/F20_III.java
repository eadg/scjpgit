/** Created on 2008-11-25 */
package chapter_07;

import java.util.*; //F19

class F20_III {
	public static void main(String[] args) {
		String[] sa = { "one", "two", "three", "four" };
		List<String> sList = Arrays.asList(sa); // make List
		System.out.println("size  " + sList.size());
		System.out.println("idx2  " + sList.get(2));
		sList.set(3, "six"); // change List
		sa[1] = "five"; // change array //F20
		for (String s : sa)
			System.out.print(s + " ");
		System.out.println("\nsList[1] " + sList.get(1));
	}
}
