/** Created on 2008-12-07 */
package chapter_07;

import java.util.*; //F34

public class F34_IV {
	public static void main(String[] args) {
		List<Integer> myList = new ArrayList<Integer>();
		Bar bar = new Bar();
		// bar.doInsert(myList); //wrong type List<Integer>
	}
}

class Bar {
	void doInsert(List<Object> list) {
		list.add(new Dog_F24());
	} // OK
		// void doInsert(List<? super Animal> list) { list.add(new Dog_F24()); }
		// //OK
		// void doInsert(List<?> list) { list.add(new Dog_F24()); }
		// void doInsert(List<? extends Animal> list) { list.add(new Dog_F24());
		// }
}
