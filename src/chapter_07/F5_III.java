/** Created on 2008-11-22 */
package chapter_07;

import java.util.*; //F5

class F5_III {
	public static void main(String[] args) {
		ArrayList<String> stuff = new ArrayList<String>(); // #1
		stuff.add("Denver");
		stuff.add("Boulder");
		stuff.add("Vail");
		stuff.add("Aspen");
		stuff.add("Telluride");
		System.out.println("unsorted " + stuff);
		Collections.sort(stuff); // #2
		System.out.println("sorted   " + stuff);
	}
} // OUT: unsorted [Denver, Boulder, Vail, Aspen, Telluride]
// sorted [Aspen, Boulder, Denver, Telluride, Vail]