/** Created on 2008-12-13 */
package chapter_07;

import java.util.*;

public class F36_IV { // F36
	private List rentalPool;

	private int maxNum;

	public F36_IV(int maxNum, List rentalPool) {
		this.maxNum = maxNum;
		this.rentalPool = rentalPool;
	}

	public Object getRental() { // blocks until there's sth available
		return rentalPool.get(0);
	}

	public void returnRental(Object o) {
		rentalPool.add(o);
	}
}