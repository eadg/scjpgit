/** Created on 2008-11-27 */
package chapter_07;

import java.util.*; //F25

class Dog_F25 {
	public Dog_F25(String n) {
		name = n;
	}

	public String name;

	public boolean equals(Object o) {
		if ((o instanceof Dog_F25) && (((Dog_F25) o).name == name)) {
			// Metoda nie jest jawnie uruchamiana...out jejst poza System.out.
			System.out.println("equals.." + ((Dog_F25) o).name);
			System.out.println(name);
			return true;
		} else {
			return false;
		}
	}

	public int hashCode() {
		return name.length();
	}

	public String toString() {
		return name;
	}
}

class Cat_F25 {
	public Cat_F25(Object s) {
		cname = s;
	}

	public Object cname;
}

enum Pets {
	Dog_F25, CAT_F25, HORSE
}

class F25_III {

	public static void main(String[] args) {
		Map<Object, Object> m = new HashMap<Object, Object>();
		m.put("k1", new Dog_F25("aiko")); // add some key/value pairs
		m.put("k2", Pets.Dog_F25);
		m.put(Pets.CAT_F25, "CAT_F25 key");
		Dog_F25 d1 = new Dog_F25("clover"); // keep this reference
		m.put(d1, "Dog_F25 key");
		Cat_F25 kot = new Cat_F25("klot");
		m.put(kot, "Cat key");
		// String k1 = "k1";
		System.out.println("#1 " + m.get("k1")); // #1
		String k2 = "k2";
		System.out.println("#2 " + m.get(k2)); // #2
		Pets p = Pets.CAT_F25;
		System.out.println("#3 " + m.get(p)); // #3
		System.out.println("#4 " + m.get(d1)); // #4
		System.out.println("#5 " + m.get(kot)); // #5
		System.out.println("#6 " + m.size()); // #6
		d1.name = "magnolia";
		System.out.println("#7 " + m.get(d1)); // 7
		d1.name = "arthur";
		System.out.println("#8 " + m.get(d1)); // 8
		d1.name = "arthur";
		System.out.println("#9 " + m.get(new Dog_F25("arthur")));

	}
}
