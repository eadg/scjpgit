/** Created on 2008-12-14 */
package chapter_07;

import java.util.*;

class Q1 {
	public static void main(String[] args) {
		// List<String> x = new LinkedList<String>();
		// LinkedList<String> x = new LinkedList<String>();
		Queue<String> x = new PriorityQueue<String>();
		x.add("one");
		x.add("two");
		x.add("TWO");
		System.out.println(x.poll());
	}
}
