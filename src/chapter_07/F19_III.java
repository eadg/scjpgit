/** Created on 2008-11-25 */
package chapter_07;

import java.util.*; //F19

class F19_III {
	public static void main(String[] args) {
		String[] sa = { "one", "two", "three", "four", "ones", "oner" };
		Arrays.sort(sa); // #1
		for (String s : sa)
			System.out.print(s + " ");
		System.out.println("\n1) one = " + Arrays.binarySearch(sa, "four")); // #2
		System.out.println("now reverse sort");
		ReSortComparator rs = new ReSortComparator(); // #3
		Arrays.sort(sa, rs);
		for (String s : sa)
			System.out.print(s + " ");
		System.out.println("\n2) one = " + Arrays.binarySearch(sa, "one")); // #4
		System.out.println("3) one = " + Arrays.binarySearch(sa, "oner", rs)); // #5
	}

	static class ReSortComparator implements Comparator<String> { // #6
		public int compare(String a, String b) {
			return b.compareTo(a); // #7
		}
	}
}
// OUT: four one three two \n one = 1 \n now reverse sort
// OUT: two three one four \n one = -1 \n one = 2