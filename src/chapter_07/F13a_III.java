package chapter_07;

import java.util.ArrayList;
import java.util.Collections;

public class F13a_III {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		ArrayList<F13_III> test = new ArrayList<F13_III>();
		test.add(new F13_III("aa", "aa", "aa"));
		test.add(new F13_III("cc", "cc", "cc"));
		test.add(new F13_III("bb", "bb", "bb"));
		System.out.println(test);
		Collections.sort(test);
		System.out.println(test);
	}

}
