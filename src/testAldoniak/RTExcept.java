package testAldoniak;

public class RTExcept {
	public static void throwit() {
		throw new RuntimeException();
	}

	public static void main(String... args) {
		try {
			System.out.println("Hello world");
			throwit();
			System.out.println("Done with try block");
		} catch (Exception e) {
			System.out.println("Catch");
		} finally {
			System.out.println("Fnally execute");
		}
	}
}
