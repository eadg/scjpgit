package testAldoniak;

//Given this scenario;
//Following code is intended to create a single WidgetMaker wich makes Widget objects and several WidgetUsers who wait for a Widget to be made.
import java.util.*;

class Widget {
}

class WidgetMaker extends Thread {
	List<Widget> finishedWidgets = new ArrayList<Widget>();

	public void run() {
		try {
			while (true) {
				Thread.sleep(2000); // act busy!
				Widget w = new Widget();
				synchronized (finishedWidgets) {
					finishedWidgets.add(w);
					finishedWidgets.notify();
				}
			}
		} catch (InterruptedException e) {
		}
	}

	public Widget itForWidget() {
		synchronized (finishedWidgets) {
			if (finishedWidgets.size() == 0) {
				try {
					finishedWidgets.wait();
				} catch (InterruptedException e) {
				}
			}
			return finishedWidgets.remove(0);
		}
	}
}

public class WidgetUser extends Thread {
	private WidgetMaker maker;

	public WidgetUser(String name, WidgetMaker maker) {
		super(name);
		this.maker = maker;
	}

	public void run() {
	}
}
/*
 * C is correct Looking at the options, this question is really comprised of t
 * questions, (1) Should synchronize on "this," on WidgetMaker.class, or on
 * finishedWidgets And (2) should use notify() or notifyAll() For the first part
 * since all the options invoke notify() or notifyAll() using finishedWidgets
 * must synchronize on finishedWidgets. "this" and WidgetMaker.class are
 * irrelent For the second part notifyAll() would be a mistake here because only
 * one of the waiting threads may use a single Widget anyway. If use notifyAll()
 * here, all three waiting threads would try to get the Widget. One of them uld
 * succeed , but the other would probably throw an ArraylndexOutoundsException,
 * because they try to get the Widget without double-checking to see if the
 * Widget is still available. (it's not.) Thus, we need to synchronize on
 * finishedWvoidgets, and use notife() rather than notifyAll().
 */