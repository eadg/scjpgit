package testAldoniak;

class Father {
	protected Father() {
		System.out.println("Created a Father");
	}
}

public class Oedipus extends Father {
	private Oedipus() {
		System.out.println("Well, that's all right then");
	}

	public static void main(String[] args) {
		new Oedipus();
	}
}
