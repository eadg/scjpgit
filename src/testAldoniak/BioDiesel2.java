package testAldoniak;

class AlternateFuel2 {
	int getRating() {
		return 42;
	}

	static int getRating2() {
		return 43;
	}

	static int i = 10;

	AlternateFuel2() {
	}

	AlternateFuel2(int i) {
		this.i = i;
	}
}

public class BioDiesel2 extends AlternateFuel2 {
	public static void main(String[] args) {
		new BioDiesel2().go();
		System.out.println(getRating2()); // #1
		// System.out.println(super.getRating2()); //#1: not allowed super for
		// static...
	}

	void go() {
		System.out.println(super.getRating()); // #2: OK
	}
}
