package testAldoniak;

class AlternateFuel {
	int getRating() {
		return 42;
	}

	static int getRating2() {
		return 43;
	}

	static int i = 10;

	AlternateFuel() {
	}

	AlternateFuel(int i) {
		this.i = i;
	}
}

public class BioDiesel extends AlternateFuel {
	BioDiesel() {
		// super(i); //it's ok
		super(getRating2()); // it's ok
	}

	public static void main(String[] args) {
		BioDiesel b = new BioDiesel();
		b.go();
		// System.out.print(super.getRating2());
	}

	void go() {
		// super(5); //illegall
		System.out.println(super.getRating());

	}
}
/*
 * jakie argumenty moze przyjmowac super. (static?) oraz super.metoda() ...
 */
