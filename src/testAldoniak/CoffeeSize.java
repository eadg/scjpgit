package testAldoniak;

enum CoffeeEnum { // F27B
	BIG(8), HUGE(10), OVERWHELMING(16) {
		public String getLidCode() {
			return "A";
		}
	}; // semicolon is REQUIRED when you have body
	CoffeeEnum(int ounces) {
		this.ounces = ounces;
	}

	private int ounces;

	public int getOunces() {
		return ounces;
	}

	public String getLidCode() {
		return "B";
	}
}

public class CoffeeSize {
	public static void main(String... arg) {
	}
}
