package testAldoniak;

class Stepper {
	enum Romain {
		l, V, X, L, C, M
	}

	public static void main(String... bang) {
		int x = 7;
		int z = 2;
		Romain r = Romain.X;
		do {
			switch (r) {
			case C:
				r = Romain.L;
				break;
			case X:
				r = Romain.C;
			case L:
				if (r.ordinal() > 2)
					z += 5;
			case M:
				x++;
			}
			z++;
			System.out.println("x " + x + " z " + z);
		} while (x < 10);
		System.out.println(z);
		System.out.println(r.ordinal());
	}
}
