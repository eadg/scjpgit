package testAldoniak.xcom;

public class Stuff {
	public static int MY_CONSTANT = 5; // must be public

	public static int doStuff(int x) {
		return (x++) * x;
	} // must be public
}
