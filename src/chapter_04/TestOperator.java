package chapter_04;

public class TestOperator {

	public static void main(String[] args) {
		int z = 5;
		if (++z > 5 | ++z > 6)
			z++;
		System.out.println(z);
		// OUT: z = 7 after this code
	}
}
