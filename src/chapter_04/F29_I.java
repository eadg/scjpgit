package chapter_04;

public class F29_I {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int sizeOfYard = 7;
		int numOfPets = 3;
		String status = (numOfPets < 2) ? "Pet count OK"
				: (sizeOfYard > 8) ? "Pet limit on edge" : " too many pets";
		System.out.println("Pet status is:" + status);

	}

}
