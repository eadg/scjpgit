/** Created on 2009-03-26 */
package chapter_04;

class A {
} // F12

public class F12_I extends A {
	public static void main(String[] args) {
		A myA = new F12_I();
		meth2(myA);
	}

	public static void meth2(A a) {
		if (a instanceof A) {
			((F12_I) a).doBstuff();
		}
		// downcast A ref to F12_I ref
		// if (a instanceof F12_I) { doBstuff(); } //OK
	}

	public void doBstuff() {
		System.out.println(" 'a' refers to a F12_I");
	}
} // OUT: 'a' refers to a F12_I
