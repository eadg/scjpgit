/** Created on 2009-03-26 */
package chapter_04;

import javax.swing.JButton; //F9

class F9A_1 {
	public static void main(String[] args) {
		JButton a = new JButton("Exit");
		JButton b = new JButton("Exit");
		JButton c = a;
		System.out.println("Is reference a == b? " + (a == b));
		System.out.println("Is reference a == c? " + (a == c));
	}
}
