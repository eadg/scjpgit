package chapter_04;

public class TestOperator1 {

	public static void main(String[] args) {
		boolean t = false;
		boolean f = true;
		System.out.println("! " + (t && f) + " " + f);
		// OUT: ! true false
	}
}
