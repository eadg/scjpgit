/** Created on 2009-03-26 */
package chapter_04;

public class F11_I {
	public static void main(String[] args) {
		String s = new String("foo"); // F11
		if (s instanceof String) {
			System.out.print("s is a String");
		}
	}
}
