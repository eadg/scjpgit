package chapter_04;

import javax.swing.JButton;

public class F6_I {

	public static void main(String[] args) {
		JButton a = new JButton("Exit"); // F6
		JButton b = a;
		if (a == b) {
			System.out.println("true");
		}

	}

}
