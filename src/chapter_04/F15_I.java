/** Created on 2009-03-26 */
package chapter_04;

class F15_I { // F15
	public static void main(String[] args) {
		String a = null;
		boolean b = null instanceof String;
		boolean c = a instanceof String;
		System.out.println(b + " " + c);
	}
}
