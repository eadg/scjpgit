/** Created on 2009-03-26 */
package chapter_04;

public class F5C_I { // F5A
	public static void main(String[] args) {
		System.out.println("#1 " + ('a' > 'A'));
		System.out.println("#2 " + ('z' > '_'));
		System.out.println("#3 " + ('_' > '1'));
		System.out.println("#4 " + (' ' > 1));
		System.out.println("#5 " + ('_' > ' '));
		System.out.println("#6 " + ('a' > 50));
		System.out.println((int) 'A');
		System.out.println((int) ' ');
		System.out.println((int) '_');
		System.out.println((int) '0');
		System.out.println("true == false? " + (true == false));
	}
}
