package chapter_04;

class F4_I { // F4
	public static void main(String[] args) {
		String animal = "unknown";
		int weight = 700;
		char sex = 'm';
		double colorWaveLength = 1.630;
		if (weight >= 500) {
			animal = "elephant";
		}
		if (colorWaveLength > 1.621) {
			animal = "gray " + animal;
		}
		if (sex <= 'M') {
			animal = "female " + animal;
		}
		System.out.println("Animal is a " + animal);
	}
}
