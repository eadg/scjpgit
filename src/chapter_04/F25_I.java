package chapter_04;

public class F25_I {
	static int players = 0;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		System.out.println("players online: " + players++);
		System.out.println("The value of players is " + players);
		System.out.println("The value of players is now " + ++players);
	}
}
