package chapter_04;

public class F28_I {
	public static void main(String... strings) {
		int numPets = 3;
		String status = (numPets < 4) ? "Pet limit not exceeded"
				: "too many pets";
		System.out.println("This pet status is " + status);
	}
} // OUT: This pet status is Pet limit not exceeded
