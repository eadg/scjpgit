/** Created on 2009-03-26 */
package chapter_04;

class Cat {
} // F16

public class F16_I {
	public static void main(String[] args) {
		F16_I dog = new F16_I();
		System.out.println(dog instanceof F16_I);
		Cat cat = new Cat();
		// System.out.println(cat instanceof F16_I); //compilation error
	}
}
