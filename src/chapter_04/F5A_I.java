/** Created on 2009-03-26 */
package chapter_04;

public class F5A_I { // F5A
	public static void main(String[] args) {
		System.out.println("char 'a' == 'a'? " + ('a' == 'a'));
		System.out.println("char 'a' == 'b'? " + ('a' == 'b'));
		System.out.println("5 != 6? " + (5 != 6));
		System.out.println("5.0f == 5L? " + (5.0f == 5L));
		System.out.println("true == false? " + (true == false));
	}
}
