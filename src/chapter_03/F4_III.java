package chapter_03;

class F4_III { // F4
	static int size = 7;

	static void changeIt(int sizez) {
		size = sizez + 200;
		System.out.println("size in changelt is " + size);
	}

	public static void main(String[] args) {
		F4_III f = new F4_III();
		System.out.println("size = " + size);
		changeIt(size);
		System.out.println("size after changeIt is " + size);
	}
}
