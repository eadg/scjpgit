package chapter_03;

class Layout { // class
	static int s = 343; // static variable

	int x; // instance variable
	{
		x = 7;
		int x2 = 5;
	} // initialization block

	Layout() {
		x += 8;
		int x = 6;
	} // constructor

	void doStuff() { // method
		int y = 0; // local variable
		for (int z = 0; z < 4; z++) { // 'for' code block
			y += z + x;
		}
	}

	public static void main(String[] args) {
		Layout lay = new Layout();
		lay.doStuff();
		System.out.println(s + " " + lay.x);
		// System.out.println(s + " " + lay2.x);
	}
}
