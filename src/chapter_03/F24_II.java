package chapter_03;

public class F24_II {
	private String title; // instance reference variable

	public String getTitle() {
		return title; // wrong line :)
	}

	public static void main(String[] args) {
		F24_II b = new F24_II();
		String s = b.getTitle(); // Compiles and runs
		String t = s.toLowerCase(); // Runtime Exception!
	}
}