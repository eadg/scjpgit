package chapter_03;

class F4_VI { // F4
	static void go(Byte x, Byte y) {
		System.out.println("Byte, Byte");
	}

	static void go(byte... x) {
		System.out.println("byte... ");
	}

	public static void main(String[] args) {
		byte b = 5;
		go(b, b);
	}
}
