package chapter_03;

class Q8 {
	int x = 5;

	public static void main(String[] args) {
		final Q8 f1 = new Q8();
		Q8 f2 = new Q8();
		Q8 f3 = FizzSwitch(f1, f2);
		System.out.println((f1 == f3) + " " + (f1.x == f3.x));
		System.out.println(f1 + " " + f1.x);
		System.out.println(f3 + " " + f3.x);
	}

	static Q8 FizzSwitch(Q8 x, Q8 y) {
		final Q8 z = x;
		z.x = 6;
		return z;
	}
}