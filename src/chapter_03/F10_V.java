package chapter_03;

public class F10_V {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String s3 = Integer.toHexString(254); // convert 254 to hex
		System.out.println("254 is " + s3); // result: "254 is fe"
		String s4 = Long.toOctalString(254); // convert 254 to octal //F10
		System.out.print("254(oct) =" + s4); // result: "254(oct) =376"

	}

}
