package chapter_03;

public class F28_II { // F28
	public static void main(String[] args) {
		int x;
		// if (args[0] != null) { // not always be true !
		if (true) { // always be true !
			x = 7; // compiler can't tell that this
		} // statement will run
		int y = x;
	}
}
