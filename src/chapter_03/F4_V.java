package chapter_03;

public class F4_V {

	public static void main(String[] args) {
		double d4 = Double.parseDouble("3.14");
		System.out.println("d4 = " + d4); // F4
		Double d5 = Double.valueOf("3.14");
		System.out.println(d5 instanceof Double);
	}

}
