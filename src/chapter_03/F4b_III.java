package chapter_03;

class F4b_III { // F4
	int size = 7;

	void changeIt(int sizez) {
		size = sizez + 200;
		System.out.println("size in changelt is " + size);
	}

	public static void main(String[] args) {
		F4b_III f = new F4b_III();
		System.out.println("size = " + f.size);
		f.changeIt(f.size);
		System.out.println("size after changeIt is " + f.size);
	}
}
