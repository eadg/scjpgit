package chapter_03;

import java.util.Date; //F8

public class F8_VIII {
	public static void main(String[] args) {
		Runtime rt = Runtime.getRuntime();
		Runtime rt2 = Runtime.getRuntime();
		System.out.println("Total JVM memory: " + rt.totalMemory());
		System.out.println("Before Memory = " + rt.freeMemory());
		Date d = null;
		for (int i = 0; i < 100000000; i++) {
			d = new Date();
			// d = null;
		}
		System.out.println("After Memory = " + rt.freeMemory());
		rt.gc(); // an alternate to System.gc()
		System.out.println("After GC Memory = " + rt.freeMemory());
		if (rt == rt2) {
			System.out.println("TRUE");
			System.out.println(rt + "\t" + rt2);
		}
	}
}
