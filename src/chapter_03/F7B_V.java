package chapter_03;

public class F7B_V {

	public static void main(String[] args) {
		char c = '\u007f';
		Character cwrap = c;
		System.out.println(cwrap);
		int i = Integer.valueOf("7f", 16);
		System.out.println(i);
		int j = Integer.parseInt("7f", 16);
		System.out.println(j);
	}

}
