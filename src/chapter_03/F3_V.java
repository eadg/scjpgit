package chapter_03;

public class F3_V {

	public static void main(String[] args) {
		Integer y = new Integer(567); // make it
		int x = y.intValue(); // unwrap it
		x++; // use it //F3
		y = new Integer(x); // re-wrap it
		System.out.println("y = " + y); // print it

	}

}
