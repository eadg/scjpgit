package chapter_03;

public class F22_II {
	int year; // Instance variable

	public static void main(String[] args) {
		F22_II bd = new F22_II();
		bd.showYear();
	}

	public void showYear() {
		System.out.println("The year is " + year);
	}
}
