package chapter_03;

public class F17_II {
	// class
	static int s = 343; // static variable

	int x; // instance variable

	int x3;
	{
		x = 7;
		int x2 = 5;
	} // initialization block

	F17_II() {
		x += 8;
		int x3 = 6;
	} // constructor

	void doStuff() { // method
		int y = 0; // local variable
		for (int z = 0; z < 4; z++) { // 'for' code block
			y += z + x;
			System.out.println(y);
		}
	}

	public static void main(String[] args) {
		F17_II lay = new F17_II();
		lay.doStuff();
		System.out.println(s + " " + lay.x);
		// System.out.println(lay.x2);
		System.out.println(lay.x3);

	}

}
