package chapter_03;

class F8_V {
	public static void main(String[] args) {
		F8_V u = new F8_V();
		System.out.println(u.go(5));
	}

	boolean go(Integer i) { // boxes the int it was passed
		Boolean ifSo = true; // boxes the literal
		Short s = 300; // boxes the primitive
		if (ifSo) { // unboxing
			System.out.println(++s); // unboxes, increments, reboxes
		}
		return !ifSo; // unboxes, returns the inverse
	}

}