package chapter_03;

class F21_IV {
	static int x;

	int y;
	static {
		x = 7;
	} // static init block
	{
		y = 8;
	} // instance init block

	public static void main(String[] args) {
		System.out.println(x);
		F21_IV si = new F21_IV();
		System.out.println(si.y);
	}
}