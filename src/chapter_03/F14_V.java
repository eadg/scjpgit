package chapter_03;

public class F14_V {

	public static void main(String[] args) {
		Integer i1 = 1000; // F6
		Integer i2 = 1000;
		if (i1 != i2)
			System.out.println("different obj i1 i2");
		if (i1.equals(i2))
			System.out.println("equal");
		Integer i3 = -128; // F6
		Integer i4 = -128; // -128 do 127
		if (i3 == i4)
			System.out.println("equal i3 i4 !!! 8-) ");
		if (i3.equals(i4))
			System.out.println("equal");
		Boolean b1 = true;
		Boolean b2 = false;
		if (b1 == b2)
			System.out.println("equal boolean objects b1 b2 !!! 8-) ");

	}

}
