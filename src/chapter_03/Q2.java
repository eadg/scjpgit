package chapter_03;

class Q2 {
	Short story = 5;

	Q2 go(Q2 cb) {
		// cb = null;
		cb.story = 6;
		cb = null;
		// cb.story=7;
		return cb;
	}

	public static void main(String[] args) {
		Q2 c1 = new Q2();
		Q2 c2 = new Q2();
		Q2 c3 = c1.go(c2);
		c1 = null;
		System.out.println(c2.story);
		c2.story = 7;
		System.out.println(c2.story);
		c2 = null;
		c2.story = 8;
		// do Stuff
	}
}