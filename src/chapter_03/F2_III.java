package chapter_03;

class Foo_F2_III {
	String name = "fuj";

	void setName(String s) {
		name = s;
	}
}

public class F2_III {

	void bar() { // F2
		Foo_F2_III f = new Foo_F2_III();
		System.out.println("bar() #1: " + f.name);
		doStuff(f);
		System.out.println("bar() #2: " + f.name);
	}

	void doStuff(Foo_F2_III g) {
		g.setName("Boo");
		System.out.println("doStuff() #3: " + g.name);
		g = new Foo_F2_III(); // Ref g wskazuje na nowy obiekt
		System.out.println("doStuff() #4: " + g.name);
	}

	public static void main(String[] args) {
		F2_III f2_obj = new F2_III();
		f2_obj.bar();
		;
	}

}
