package chapter_03;

public class F9_IV_B {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[] x = new int[5]; // F4
		x[4] = 2; // OK, last element is at index 4
		x[5] = 3; // Runtime exception. There is no element at index
		int[] z = new int[2];
		int y = -3;
		z[y] = 4; // Runtime exception.; y is negative number

	}

}
