package chapter_03;

class Car {
}

class Honda extends Car {
}

class Beer {
}

public class F17_IV {
	public static void main(String[] args) {
		Car[] cars;
		Honda[] cuteCars = new Honda[5];
		cars = cuteCars; // OK because Honda is a type of Car
		Beer[] beers = new Beer[99];
		// cars = beers; // NOT OK, Beer is not a type of Car
		// cuteCars = cars; //Incompatible type
	}
}
