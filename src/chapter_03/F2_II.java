package chapter_03;

class F2_II {
	public static void main(String[] args) {
		int x = 0x0001;
		int y = 0x7fffffff;
		int z = 0xDeadCafe;
		String s = Integer.toString(y, 16);
		System.out.println("x = " + x + " y = " + y + " z = " + z);
		System.out.println(s);

		int zz = Integer.valueOf(s, 16);
		System.out.println(zz);
		zz = Integer.parseInt(s, 16);
		System.out.println(zz);
	}
}