package chapter_03;

class Q6 {
	Q6() {
	}

	Q6(Q6 m) {
		m1 = m;
	}

	Q6 m1;

	public static void main(String[] args) {
		Q6 m2 = new Q6();
		Q6 m3 = new Q6(m2);
		m3.go();
		Q6 m4 = m3.m1;
		m4.go();
		Q6 m5 = m2.m1;
		m5.go();
	}

	void go() {
		System.out.print("hi ");
	}
}