package chapter_03;

public class F5B_V {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Integer y = 567; // F5
		Integer x = y;
		System.out.println(y == x);
		y++; // Create new object
		System.out.println(x + " " + y);
		System.out.println(y == x);
		y--;
		System.out.println(x + " " + y);
		System.out.println(y == x);

		// OUT: true | 567 568 | false
	}
}
