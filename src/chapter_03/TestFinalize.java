package chapter_03;

class TryCatchFinallyFinalizeTest implements Runnable {

	private void testMethod() throws InterruptedException {
		try {
			System.out
					.println("In try block " + Thread.currentThread().getId());
			throw new NullPointerException();
		} catch (NullPointerException npe) {
			System.out.println("In catch block "
					+ Thread.currentThread().getId());
		} finally {
			System.out.println("In finally block "
					+ Thread.currentThread().getId());
		}
	}

	@Override
	protected void finalize() throws Throwable {
		System.out.println("In finalize block "
				+ Thread.currentThread().getId());
		super.finalize();
	}

	@Override
	public void run() {
		try {
			testMethod();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}

public class TestFinalize {

	public static void main(String[] args) {
		for (int i = 1; i <= 20; i++) {
			new Thread(new TryCatchFinallyFinalizeTest()).start();
			System.gc();
		}
	}
}
