package chapter_03;

public class F11_IV {
	void takesAnArray(int[] someArray) {
		System.out.println(someArray[2]);
	}

	public static void main(String[] args) {
		F11_IV f = new F11_IV();
		f.takesAnArray(new int[] { 7, 7, 8, 2, 5 }); // we need an array
														// argument
	}
}
