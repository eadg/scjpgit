package chapter_03;

class F22_IV_mother {
	F22_IV_mother() {
		System.out.println("MOTHER: no-arg const");
	}

	static {
		System.out.println("MOTHER: 1st static init");
	}
	{
		System.out.println("MOTHER: 1st instance init");
	}
	{
		System.out.println("MOTHER: 2nd instance init");
	}
	static {
		System.out.println("MOTHER: 2nd static init");
	}
}

public class F22_IV extends F22_IV_mother {
	F22_IV(int x) {
		System.out.println("DAUGHTER: 1-arg const");
	}

	F22_IV() {
		System.out.println("DAUGHTER: no-arg const");
	}

	static {
		System.out.println("DAUGHTER: 1st static init");
	}
	{
		System.out.println("DAUGHTER: 1st instance init");
	}
	{
		System.out.println("DAUGHTER: 2nd instance init");
	}
	static {
		System.out.println("DAUGHTER: 2nd static init");
	}

	public static void main(String[] args) {
		new F22_IV();
		System.out.println("aaaaaaaaaaaaaaaa");
		new F22_IV(7);
	}
}
