package chapter_03;

public class F9_VI {
	static void wide_vararg(long... x) {
		System.out.println("long...");
	}

	static void box_vararg(Integer... x) {
		System.out.println("Integer...");
	}

	public static void main(String[] args) {
		int i = 5;
		wide_vararg(5, 5); // needs to widen and use var-args
		box_vararg(5, 5); // needs to box and use var-args
	}
}
