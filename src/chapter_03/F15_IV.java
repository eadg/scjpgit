package chapter_03;

interface Sporty {
	void beSporty();
}

class Ferrari extends Car_1 implements Sporty {
	public void beSporty() {
	}
}

class RacingFlats extends AthleticShoe implements Sporty {
	public void beSporty() {
	}
}

class GolfClub {
}

class F15_IV {
	public static void main(String[] args) {
		Sporty[] sportyThings = new Sporty[3];
		sportyThings[0] = new Ferrari(); // OK, Ferrari, implements Sporty
		sportyThings[1] = new RacingFlats(); // OK, RacingFlats, implements
												// Sporty
		// !! //sportyThings[2] = new GolfClub();
		// Not OK; GolfClub does not implement Sporty I don't care what anyone
		// says
	}
}
