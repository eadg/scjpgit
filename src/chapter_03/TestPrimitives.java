package chapter_03;

public class TestPrimitives {

	public static void main(String[] args) {
		char c = 'D';
		Character co = new Character(c);
		int i = Character.digit(c, 2);
		System.out.println(Character.digit(co, 10));
		System.out.println(co.toString());
		// =====================//
		Character charo2 = new Character(c);
		System.out.println("1)" + charo2.toString());
		System.out.println("2)" + Character.toString(c));
		Double dou1 = new Double("3.14");
		System.out.println("1)" + dou1.toString());
		System.out.println("2)" + Double.toString(3.14));
		long ll = 4L;
	}
}
