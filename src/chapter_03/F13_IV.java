package chapter_03;

public class F13_IV {
	public static void main(String[] args) {
		int[] weightList = new int[5];
		byte b = 4;
		char c = 'c'; // F13
		short s = 7;
		weightList[0] = b;
		weightList[1] = c;
		weightList[2] = s;
		for (int i : weightList) {
			System.out.println(i);
		}
	}
}
