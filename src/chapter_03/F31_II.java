package chapter_03;

class F31_II {
	public static void main(String[] args) {
		String x = "Java"; // Assign a value to x
		String y = x; // Now y and x refer to the same String object
		System.out.println("y string = " + y);
		x = x + " Bean"; // Now modify the object using the x reference
		System.out.println("y string = " + y);
		System.out.println("x string = " + x);
		String s = "Fred";
		String t = s; // Now t and s refer to the same String object
		String t2 = t.toUpperCase(); // Invoke a String method that changes
										// String
		System.out.println(t);
		System.out.println(s);
		System.out.println(t2);
	}
}