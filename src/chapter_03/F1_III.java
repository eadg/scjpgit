package chapter_03;

import java.awt.Dimension; //F1

class F1_III {
	public static void main(String[] args) {
		Dimension d = new Dimension(5, 10);
		F1_III rt = new F1_III();
		System.out.println("Before modify d.height = " + d.height);
		rt.modify(d);
		System.out.println("After modify d.height = " + d.height);
	}

	void modify(Dimension dim) {
		dim.height = dim.height + 1;
		System.out.println("dim = " + dim.height);
	}
}
