package chapter_03;

public class F5_V {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		long L2 = Long.parseLong("101010", 2);
		System.out.println("L2 = " + L2);
		Long L3 = Long.valueOf("101010", 2);
		System.out.println("L3 value = " + L3);
		Long L4 = Long.valueOf("20");
		System.out.println("L3 value = " + L4);
		Long L5 = Long.valueOf("1019E", 110);
		System.out.println("L3 value = " + L5);

	}

}
