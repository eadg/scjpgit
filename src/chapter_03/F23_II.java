package chapter_03;

public class F23_II {
	private String title; // instance reference variable

	public String getTitle() {
		return title;
	}

	public static void main(String[] args) {
		F23_II b = new F23_II();
		System.out.println("The title is " + b.getTitle());
	}
}
