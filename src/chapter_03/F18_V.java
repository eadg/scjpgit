package chapter_03;

import java.util.*;

public class F18_V {
	public static void main(String... strings) {
		ArrayList<Integer> integerList = new ArrayList<Integer>();
		integerList.add(Integer.valueOf(10));
		integerList.add(Integer.valueOf("10"));
		integerList.add(Integer.valueOf("101010", 2));
		for (Integer i : integerList)
			System.out.println(i);
	}
}
