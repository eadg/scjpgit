package chapter_03;

public class F25_II { // F25
	private String title; // instance reference variable

	public String getTitle() {
		return title;
	}

	public static void main(String[] args) {
		F25_II b = new F25_II();
		String s = b.getTitle(); // Compiles and runs
		if (s != null) {
			String t = s.toLowerCase();
		}
	}
}
// /RUN_OUT: OK

