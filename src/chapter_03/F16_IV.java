package chapter_03;

public class F16_IV {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[] splats;
		int[] dats = new int[4];
		char[] letters = new char[5]; // F16
		splats = dats; // OK, dats refers to int array
		// splats = letters; //BAD, letters is char array

	}

}
