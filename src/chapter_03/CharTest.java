package chapter_03;

public class CharTest {

	public static void main(String[] args) {
		char a = 0x892; // hexadecimal literal
		char b = 19; // int literal
		char c = (char) 70000; // The cast is required; 70000 is
		char letterN = '\u004E'; // The letter 'N'
		System.out.println(letterN);
		char d = '�';
		int i = d;
		System.out.println(i);
	}
}
