package chapter_03;

import java.util.*;

public class F12_V {
	public static void main(String... strings) {
		ArrayList<String> stringList = new ArrayList<String>();
		stringList.add(Integer.toString(10));
		stringList.add(Integer.toString(101010, 2));
		for (String i : stringList)
			System.out.println(i);

		/* toString */
		System.out.println("toString 1: " + Integer.toString(10, 2));
	}
}
