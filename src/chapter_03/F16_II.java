package chapter_03;

class F16_II_Foo {

	public void doFooStuff() {
	}
}

class F16_II_Bar extends F16_II_Foo {
	public void doBarStuff() {
	}

}

public class F16_II {
	public static void main(String[] args) {
		F16_II_Foo reallyABar = new F16_II_Bar(); // Legal because Bar is
													// subclass of Foo
		// F16_II_Bar reallyAFoo = new F16_II(); // Illegal! Foo is not subclass
		// of Bar
	}
}
