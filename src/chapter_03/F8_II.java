package chapter_03;

public class F8_II {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// int x = 3957.229; // illegal. Explicit cast needed
		int x = (int) 3957.229; // legal cast
		long l = 4433334444444433003L;
		float f = l;

	}

}
