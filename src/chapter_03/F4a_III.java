package chapter_03;

class F4a_III { // F4
	static int size = 7;

	static void changeIt(int size) {
		size = size + 200;
		System.out.println("size in changelt is " + size);
	}

	public static void main(String[] args) {
		F4a_III f = new F4a_III();
		System.out.println("size = " + size);
		changeIt(size);
		System.out.println("size after changeIt is " + size);
	}
}
