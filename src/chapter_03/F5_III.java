package chapter_03;

class Bar_F5_III {
	int barNum = 28;
} // F5

class F5_III {
	Bar_F5_III myBar = new Bar_F5_III();

	void changeIt(Bar_F5_III myBar) {
		myBar.barNum = 99;
		System.out.println("myBar.barNum in changeIt is " + myBar.barNum);
		myBar = new Bar_F5_III();
		myBar.barNum = 420;
		System.out.println("myBar.barNum in changelt is now " + myBar.barNum);
	}

	public static void main(String[] args) {
		F5_III f = new F5_III();
		System.out.println("f.myBar.barNum is " + f.myBar.barNum);
		f.changeIt(f.myBar);
		System.out
				.println("f.myBar.barNum after changeIt is " + f.myBar.barNum);
	}
}