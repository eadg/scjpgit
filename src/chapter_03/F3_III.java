package chapter_03;

public class F3_III {
	public static void main(String[] args) {
		int a = 1;
		F3_III rt = new F3_III();
		System.out.println("Before modify() = " + a);
		rt.modify(a);
		System.out.println("After modify() = " + a);
		rt.modify2(a);
		System.out.println("After modify() = " + a);
	}

	void modify(int number) {
		number = number + 1;
		System.out.println("number = " + number);
	}

	int modify2(int number) {
		number = number + 1;
		System.out.println("number = " + number);
		return number;
	}

}
