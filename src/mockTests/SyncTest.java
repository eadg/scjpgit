package mockTests;

public class SyncTest {
	private static Foo_SyncTest foo = new Foo_SyncTest();

	static class Increaser extends Thread {
		public void run() {
			foo.increase(20);
		}
	}

	public static void main(String[] args) {
		new Increaser().start();
		new Increaser().start();
		new Increaser().start();
	}
}

class Foo_SyncTest {
	private int data = 23;

	public synchronized void increase(int a) {
		data = data + a;
		System.out.println("Data is, " + data);
	}
}
