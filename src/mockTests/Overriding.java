package mockTests;

import java.util.*;

class C3 {
	public ArrayList doIt() {
		ArrayList lst = new ArrayList();
		System.out.println("super doIt()");
		return lst;
	}

}

public class Overriding extends C3 {
	public ArrayList doIt()
	// public List doIt() - Compile Error
	{
		List lst = new ArrayList();
		System.out.println("subclas doIt()");
		return (ArrayList) lst;
	}

	public static void main(String... args) {
		C3 sb = new Overriding();
		sb.doIt();
	}
}
/*
 * What is the output ? 1 subclas doIt() 2 super doIt() 3 Compile with error 4
 * None of the above You have choosen : 3 Correct Answer is : 3 You were
 * correct. Explanations : The return type is incompatible with
 * SuperClass.doIt(). subtype return is eligible in jdk 1.5 for overidden method
 * but not super type return. super class method return ArrayList so subclass
 * overriden method should return same or sub type of ArrayList.
 */
