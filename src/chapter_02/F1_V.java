package chapter_02;

import java.io.*;

public class F1_V { // F1
	public void doStuff(int y, String s) {
	}

	public void moreThings(int x) {
	}
}

class F1_V_Bar extends F1_V {
	public void doStuff(int y, String s) {
	}

	public void doStuff(String s, int y) {
	}

	protected void doStuff(int y, long s) throws IOException {
	}
	// protected void doStuff(String s, int y) throws IOException { }
	// protected String doStuff(int y, long s) throws IOException { return "a";
	// }

}