package chapter_02;

class Animal2 { // F4
	public void eat() throws Exception {
		System.out.println("Animal2");
	}
	// public void eat() {System.out.println("Animal2");}
}

class F4_IV_b extends Animal2 {
	public void eat() {
		System.out.println("Dog2Animal2");
	} // throws Exception {}

	// public void eat() throws Exception {}
	// {System.out.println("Dog2Animal2");}
	public static void main(String[] args) {
		Animal2 ani = new Animal2();
		Animal2 ani2 = new F4_IV_b();
		F4_IV_b d = new F4_IV_b();
		try {
			ani.eat();
			ani2.eat();
			d.eat();
		} catch (Exception e) {
		}
		// ani.eat(); //unhandled exceptions
		// ani2.eat(); //unhandled exceptions
		d.eat();
	}
}