package chapter_02;

class Uber {
	static int y = 2;

	int x;

	Uber(int x) {
		this();
		y = y * 2;
		this.x = x;
	}

	Uber() {
		y++;
	}
}

class Q12 extends Uber {
	Q12() {
		super(y);
		y = y + 3;
	}

	public static void main(String[] args) {
		new Q12();
		System.out.println(y);
	}
}
