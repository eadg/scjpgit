package chapter_02;

class F6_Frog_X { // F6
	static int frogCount = 0;

	// Declare and initialize static variable
	public F6_Frog_X() {
		F6_Frog_X.frogCount += 1;
	}
	// Modify value in constructor
}

public class F6_X {
	public static void main(String[] args) {
		new F6_Frog_X();
		new F6_Frog_X();
		new F6_Frog_X();
		System.out.print("fCount: " + F6_Frog_X.frogCount);
	}
}
