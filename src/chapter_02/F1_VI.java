package chapter_02;

class Animal1_VI {
	void makeNoise() {
		System.out.println("generic noise by Animal");
	}
}

class Dog1_VI extends Animal1_VI {
	void makeNoise() {
		System.out.println("makeNoise by Dog");
	}

	void playDead() {
		System.out.println("playDead");
	}
}

class F1_VI {
	public static void main(String[] args) {
		Animal1_VI[] a = { new Animal1_VI(), new Dog1_VI(), new Animal1_VI() };
		for (Animal1_VI animal : a) {
			animal.makeNoise();
			if (animal instanceof Dog1_VI) {
				// animal.playDead(); // try to do a Dog1_VI behavior ?
				((Dog1_VI) animal).playDead();
			}
		}
	}
}
