package chapter_02;

class Animal4 {
} // F2

class Horse4 extends Animal4 {
}

public class F2_V {
	public void doStuff(Animal4 a) {
		System.out.println("In Animal version");
	}

	public void doStuff(Horse4 h) {
		System.out.println("In Horse version");
	}

	public static void main(String[] args) {
		F2_V ua = new F2_V();
		Animal4 animalobj = new Animal4();
		Horse4 horseobj = new Horse4();
		Animal4 animalhorseobj = new Horse4();
		ua.doStuff(animalobj);
		ua.doStuff(horseobj);
		ua.doStuff(animalhorseobj);
	}
}