package chapter_02;

class Animal3A_V { // F3
	public void eat() {
		System.out.println("Generic Animal Eating Generically");
	}

	public void eat(String s) {
		System.out.println("Generic Animal eating " + s);
	}
}

class Horse3A_V extends Animal3A_V {
	public void eat() {
		System.out.println("Horse eating hay ");
	}

	public void eat(String s) {
		System.out.println("Horse eating " + s);
	}
}

public class F3A_V {
	public static void main(String... args) {
		Animal3A_V a1 = new Animal3A_V();
		a1.eat();
		Horse3A_V h1 = new Horse3A_V();
		h1.eat();
		Animal3A_V a2 = new Horse3A_V();
		a2.eat();
		Animal3A_V a3 = new Horse3A_V();
		a3.eat("sasdfas");
	}
}