package chapter_02;

public class F11_IX_Animal {
	String name;

	F11_IX_Animal(String name) {
		this.name = name;
	}

	F11_IX_Animal() {
		this(makeRandomName());
		// this("aa");
	}

	static String makeRandomName() {
		int x = (int) (Math.random() * 5);
		String name = new String[] { "Fluffy", "Fido", "Rover", "Spike", "Gigi" }[x];
		return name;
	}

	public static void main(String[] args) {
		F11_IX_Animal a = new F11_IX_Animal();
		System.out.println(a.name);
		if (a instanceof Object) {
			System.out.println("true");
		}
		F11_IX_Animal b = new F11_IX_Animal("Zeus");
		System.out.println(b.name);
	}
}