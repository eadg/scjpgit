package chapter_02;

class F4_VI_Animal {
} // F4

class F4_VI_Dog extends F4_VI_Animal {
}

class F4_VI {
	public static void main(String[] args) {
		F4_VI_Dog d = new F4_VI_Dog();
		F4_VI_Animal a1 = d; // upcast ok with no explicit cast
		F4_VI_Animal a2 = (F4_VI_Animal) d; // upcast ok with explicit cast
	}
}
