package chapter_02;

import java.util.*;

public class Test1 {
	public static void main(String[] args) {
		Integer int1 = new Integer(10);
		String s1 = "asdf";
		Vector vec1 = new Vector();
		LinkedList list = new LinkedList();
		vec1.add(int1);
		list.add(int1);
		vec1.add(s1);
		list.add(s1);
		vec1.add(new Ex07());
		list.add(new Ex07());
		if (vec1.equals(list))
			System.out.println("equal");
		else
			System.out.println("not equal");
		System.out.println(vec1.hashCode());
		System.out.println(list.hashCode());
	}
}