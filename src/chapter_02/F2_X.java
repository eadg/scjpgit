package chapter_02;

class F2_X { // F2
	int frogCount = 0;

	// Declare and initialize instance variable
	public F2_X() {
		frogCount += 1;
		// Modify value in constructor
	}

	public static void main(String[] args) {
		new F2_X();
		new F2_X();
		new F2_X();
		// System.out.println ("Count=" + frogCount);
	}
}
