package chapter_02;

class F2_Foo {
	void go() {
	}
} // F2

public class F2_VIII extends F2_Foo {
	// String go() { return null;}
	// Not legal! Can't change only return type
}
