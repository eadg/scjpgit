package chapter_02;

public class ReturnPrimitive {
	public int foo() {
		char c = 'c';
		long l = 100;
		byte b = 120;
		short s = 250;
		return s;
	}

	public static void main(String[] args) {
	}
}
// OK for any except long l