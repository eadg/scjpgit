package chapter_02;

class F5_X { // F5
	int frogSize = 0;

	public int getFrogSize() {
		return frogSize;
	}

	public F5_X(int s) {
		frogSize = s;
	}

	public static void main(String[] args) {
		F5_X f = new F5_X(25);
		System.out.println(f.getFrogSize());
	}

}
