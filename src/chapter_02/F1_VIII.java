package chapter_02;

class Foo2_VIII {
	void go() {
	} // F1
}

class Bar_VIII extends Foo2_VIII {
	String go(int x) {
		return null;
	}
}

public class F1_VIII {
	public static void main(String[] args) {
		Bar_VIII b = new Bar_VIII();
		b.go(2);
	}
}