package chapter_02;

public class F3_IV { // F3
	public static void main(String[] args) {
		F3_IV_Horse h = new F3_IV_Horse();
		// h.eat(); //Not work because F3_IV_Horse private Animal.eat()
	}
}

class F3_IV_Animal {
	private void eat() {
		System.out.println("Generic An. Eati. sth");
	}
}

class F3_IV_Horse extends F3_IV_Animal {
}
