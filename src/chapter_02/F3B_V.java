package chapter_02;

class Animal3B_V { // F1
	public void eat() {
		System.out.println("Generic Animal Eating Gene");
	}
}

class Horse extends Animal3B_V {
	public void eat() {
		System.out.println("Horse eating sth," + "and etc");
	}

	public void eat(String s) {
		System.out.println("Horse eating sth and " + s);
	}

	public void buck() {
		System.out.println("Invocation buck()");
	}
}

public class F3B_V {
	public static void main(String[] args) {
		Animal3B_V a = new Animal3B_V();
		Animal3B_V b = new Horse();
		a.eat(); // Runs Animal3 version of eat()
		b.eat(); // Runs Horse version of eat()
		Animal3B_V c = new Horse();
		// c.buck(); // Can't invoke buck();
		((Horse) c).buck();
	}
}
