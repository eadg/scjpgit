package chapter_02;

class A {
	A() {
		this("foo");
	}

	A(String s) {
		// remark, because error compile
		// this();
	}
}
