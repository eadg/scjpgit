package chapter_02;

interface Bounceable extends Moveable, Spherical { // ok!
	void bounce();

	void setBounceFactor(int bf);
}

interface Moveable {
	void moveIt();
}

interface Spherical {
	void doSphericalThing();
}

public class F2_VII implements Bounceable {
	public void moveIt() {
	}

	public void setBounceFactor(int bf) {
	}

	public void doSphericalThing() {
	}

	public void bounce() {
	}
}