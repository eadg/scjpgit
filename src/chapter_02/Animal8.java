package chapter_02;

public class Animal8 { // F11
	String name;

	Animal8(String name) {
		this.name = name;
	}

	Animal8() {
		this(makeRandomName());
	}

	static String makeRandomName() {
		int x = (int) (Math.random() * 5);
		String name = new String[] { "Fluffy", "Fido", "Rover", "Spike", "Gigi" }[x];
		System.out.println(x);
		return name;
	}

	public static void main(String[] args) {
		Animal8 an = new Animal8();
		System.out.println(an.name);
		Animal8 b = new Animal8("Zeus");
		System.out.println(b.name);
	}
} // OUT: Fido||Rover... \n Zeus