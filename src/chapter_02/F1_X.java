package chapter_02;

class F1_X { // F1
	static int frogCount = 0;

	// Declare and initialize static variable
	public F1_X() {
		frogCount += 1;
		// Modify value in constructor
	}

	public static void main(String[] args) {
		new F1_X();
		new F1_X();
		new F1_X();
		System.out.println("Count=" + frogCount);
	}
} // OUT: Frog count is now 3

