package chapter_02;

class F8_Animal_X {
	static void doStuff() { // F8
		System.out.print("a ");
	}
}

class F8_X extends F8_Animal_X {
	static void doStuff() { // it's redefinition, not override
		System.out.print("d ");
	}

	public static void main(String[] args) {
		F8_Animal_X[] a = { new F8_Animal_X(), new F8_X(), new F8_Animal_X() };
		for (int x = 0; x < a.length; x++)
			// {}
			a[x].doStuff(); // invoke static method
	}
}
