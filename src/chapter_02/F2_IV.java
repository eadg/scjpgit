package chapter_02;

public class F2_IV { // F2
	public static void main(String[] args) {
		F2_IV_Animal a = new F2_IV_Animal();
		F2_IV_Animal b = new F2_IV_Horse();
		a.eat(); // Runs F2_IV_Animal version of eat()
		b.eat(); // Runs F2_IV_Horse version of eat()
	}
}

class F2_IV_Animal {
	public void eat() {
		System.out.println("Generic.. Generically");
	}
}

class F2_IV_Horse extends F2_IV_Animal {
	// private void eat() { // whoa! - it's private!
	public void eat() { // must be public
		System.out.println("F2_IV_Horse");
	}
}
