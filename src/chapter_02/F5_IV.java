package chapter_02;

class Animal_F5_IV { // F5
	void eat() {
		System.out.println("Animal_F5_IV");
	}
}

public class F5_IV extends Animal_F5_IV {

	// public String eat() { System.out.println("F5_IV"); return "F5_IV"; }

	public static void main(String... args) {
		Animal_F5_IV f5obj = new F5_IV();
		f5obj.eat();
	}
}
