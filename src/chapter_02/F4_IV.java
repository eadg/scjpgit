package chapter_02;

class F4_IV_Animal2 { // F4
	public void eat() {
		System.out.println("F4_IV_Animal2");
	}
}

class F4_IV extends F4_IV_Animal2 {
	// public void eat() throws Exception {}
	// {System.out.println("Dog2Animal2");}
	// compilation fail in realtion above
	public static void main(String[] args) {
		F4_IV_Animal2 ani = new F4_IV_Animal2();
		F4_IV_Animal2 ani2 = new F4_IV();
		F4_IV d = new F4_IV();
		try {
			ani.eat();
			ani2.eat();
			d.eat();
		} catch (Exception e) {
		}
		// ani.eat(); //unhandled exceptions
		// ani2.eat(); //unhandled exceptions
	}
}