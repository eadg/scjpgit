package chapter_02;

class Clothing {
	String name_c;

	Clothing(String s) {
		name_c = s + "" + s;
		System.out.println("In Clothing");
	}
}

public class F9_IX_A extends Clothing {
	String name_t;

	F9_IX_A(String a) {
		super(a);
		name_t = a;
		System.out.println("In Tshirt");
	}

	public static void main(String[] args) {
		F9_IX_A ts1 = new F9_IX_A("cotton");
		System.out.println(ts1.name_t);
		System.out.println(ts1.name_c);
	}
}
