package chapter_02;

class Animal_F2_VI {
} // F2

class Dog_F2_VI extends Animal_F2_VI {
}

public class F2_VI {
	public static void main(String[] args) {
		Animal_F2_VI animal = new Animal_F2_VI();
		Dog_F2_VI d = new Dog_F2_VI();
		d = (Dog_F2_VI) animal;
		// compiles but fails later: java.lang.ClassCastException:
		// thrash.Animal6
	}
}
