package chapter_08;

public class Q1 {
	public static class MyInner {
		public static void foot() {
		}
	}

	Q1.MyInner m1 = new Q1.MyInner();

	MyInner m2 = new Q1.MyInner();

	Q1.MyInner m3 = new MyInner();

	Q1 m4 = new Q1();
	// MyInner m5 = m4.Q1.MyInner();

}
