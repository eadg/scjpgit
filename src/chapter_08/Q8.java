/** Created on 2009-01-03 */
package chapter_08;

public class Q8 {
	Q8() {
		System.out.print("foo");
	}

	class Bar {
		Bar() {
			System.out.print("bar");
		}

		public void go() {
			System.out.print("hi");
		}
	}

	public static void main(String[] args) {
		Q8 f = new Q8();
		f.makeBar();
	}

	void makeBar() {
		(new Bar() {
		}).go();
	}
}
