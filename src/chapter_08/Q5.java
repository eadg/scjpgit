package chapter_08;

class FooQ5 {
	class BarQ5 {
	}
}

public class Q5 {
	public static void main(String[] args) {
		FooQ5 f = new FooQ5();
		FooQ5.BarQ5 b1 = new FooQ5().new BarQ5();
		// FooQ5.BarQ5 b2 = new FooQ5.BarQ5();
		FooQ5.BarQ5 b2 = f.new BarQ5();
		// BarQ5 b3 = new f.BarQ5();
		// BarQ5 b4 = f.new BarQ5();
		// FooQ5.BarQ5 b5 = new f.BarQ5();
	}
}
