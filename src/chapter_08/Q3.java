package chapter_08;

interface Runnabless {
	void run();
}

public class Q3 {
	Runnabless r = new Runnabless() {
		public void run() {
		}
	};

	public static void main(String... strings) {
		System.out.println(new Runnabless() {
			public void run() {
			}
		});
	}
}