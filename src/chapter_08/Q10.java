/** Created on 2009-01-04 */
package chapter_08;

class Q10 {
	public static void main(String[] args) {
		class Horse {
			final String name;

			int i = 1;

			public Horse(String s) {
				name = s;
			}
		}
		Object obj = new Horse("Zippo");
		Horse h = (Horse) obj;
		System.out.println(h.name);
	}
}
