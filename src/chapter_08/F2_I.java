package chapter_08;

class F2_I { // F2
	private int x = 7; // IC definition

	class MyInner {
		public void seeOuter() {
			System.out.println("Outer x is " + x);
		}
	} // close IC definition

	public static void main(String... strings) {
		F2_I.MyInner inner = new F2_I().new MyInner(); // F4
		inner.seeOuter();
	}
} // close OC

