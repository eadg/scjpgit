/** Created on 2008-12-30 */
package chapter_08;

class F10_III {
	void go() { // F10
		Bar b = new Bar();
		b.doStuff(new Foo() {
			public void foof() {
				System.out.println("foofy");
			}
		}); // end IC def, arg, and b.doStuff
	} // end go()

	public static void main(String[] args) {
		F10_III f10 = new F10_III();
		f10.go();
	}
} // end class 11.//

interface Foo {
	void foof();
}

class Bar {
	void doStuff(Foo f) {
	}
}
