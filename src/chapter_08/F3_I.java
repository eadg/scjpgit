/** Created on 2008-12-20 */
package chapter_08;

class F3_I { // F3
	private int x = 7;

	public void makeInner() {
		MyInner in = new MyInner(); // make inner instance
		in.seeOuter();
	}

	class MyInner {
		public void seeOuter() {
			System.out.println("Private Outer x is " + x);
		}
	}

	public static void main(String[] args) {
		F3_I myOu = new F3_I(); // #1
		myOu.makeInner();
		F3_I.MyInner myOu2 = new F3_I().new MyInner(); // #2
		myOu2.seeOuter();
		F3_I.MyInner myOu3 = myOu.new MyInner(); // #3
		myOu3.seeOuter();
	}
}
