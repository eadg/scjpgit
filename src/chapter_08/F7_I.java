/** Created on 2008-12-20 */
package chapter_08;

class F7_I { // F7
	private int x = 7;

	public void makeInner() {
		MyInner in = new MyInner();
		in.seeOuter();
	}

	class MyInner {
		public int y = 77;

		public void seeOuter() {
			System.out.println("Outer x is " + x);
			System.out.println("Inner class ref is " + this.y); // overkill!
			System.out.println("Outer class ref is " + F7_I.this.x); // overkill!!
		}
	}

	public static void main(String[] args) {
		F7_I outer = new F7_I();
		outer.makeInner();
	}
}
