/** Created on 2009-01-04 */
package chapter_08;

public abstract class Q12 {
	public int getNum() {
		return 45;
	}

	public abstract class Q12Bar {
		public int getNum() {
			return 38;
		}
	}

	public static void main(String[] args) {
		Q12 t = new Q12() {
			public int getNum() {
				return 22;
			}
		};
		Q12.Q12Bar f = t.new Q12Bar() {
			public int getNum() {
				return 57;
			}
		};
		System.out.println(f.getNum() + " " + t.getNum());
	}
}
