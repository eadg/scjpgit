/** Created on 2009-01-03 */
package chapter_08;

class Boo {
	Boo(String s) {
	}

	Boo() {
	}
}

public class Q4 extends Boo {
	Q4() {
	}

	Q4(String s) {
		super(s);
	}

	void zoo() {
		Boo f1 = new Boo() {
			String s;
		};
		Boo f2 = new Q4() {
		};
	}
}
