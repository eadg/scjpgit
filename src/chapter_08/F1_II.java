/** Created on 2008-12-20 */
package chapter_08;

class F1_II { // F1
	private String x = "Outer2";

	void doStuff() {
		class MyInner {
			public void seeOuter() {
				System.out.println("Outer x is " + x);
			}
		}
		MyInner mi = new MyInner();
		mi.seeOuter();
	}

	public static void main(String[] args) {
		F1_II myOu = new F1_II();
		myOu.doStuff();
	}
}
