/** Created on 2008-12-28 */
package chapter_08;

class BigOuter { // F2
	static void setBig() {

	}

	static class Nest {
		void go() {
			System.out.println("hi");
		}
	}
}

public class F2_IV {
	static int a = 666;

	static class B2 {
		void goB2() {
			System.out.println("hi 2");
			System.out.println(a);
		}
	}

	public static void main(String[] args) {
		BigOuter.Nest n = new BigOuter.Nest();
		n.go();
		F2_IV.B2 b2 = new F2_IV.B2();// access enclosed class
		b2.goB2();
	}
} // OUT: hi \n hi 2
