/** Created on 2008-12-30 */
package chapter_08;

class F10_III_a {
	void go() { // F10
		Bar_a b = new Bar_a();
		b.doStuff(new Q8() {
			public void foof() {
				System.out.println("foofy");
			}
		}); // end IC def, arg, and b.doStuff
	} // end go()
} // end class 11.//

interface Foo_a {
	void foof();
}

class Bar_a {
	void doStuff(Q8 f) {
	}
}
