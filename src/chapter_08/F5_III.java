/** Created on 2008-12-30 */
package chapter_08;

class Popcorn_F5 { // F5
	public void pop() {
		System.out.println("popcorn");
	}
	// public void sizzle () { System.out.println("sizzle"); }
}

class Foo_F5 {
	Popcorn_F5 p = new Popcorn_F5() {
		public void sizzle() {
			System.out.println("anonymous sizzling popcorn");
		}

		public void pop() {
			System.out.println("anonymous popcorn");
		}
	};

	public void popIt() {
		p.pop(); // OK, Popcorn has pop() method
		// p.sizzle(); //Illegal! Popcorn does not have sizzle()
	}
}
