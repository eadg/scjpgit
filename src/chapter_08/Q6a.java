//: innerclasses/Parcel8.java
package chapter_08; /* Added by Eclipse.py */

// Calling the base-class constructor.
class Q6aWrapping {
	private int i;

	public Q6aWrapping(int x) {
		i = x;
	}

	public int value() {
		return i;
	}
} // /:~

public class Q6a {
	public Q6aWrapping wrapping(int x) {
		// Base constructor call:
		return new Q6aWrapping(x) { // Pass constructor argument.
			public int value() {
				return super.value() * 47;
			}
		}; // Semicolon required
	}

	public static void main(String[] args) {
		Q6a p = new Q6a();
		Q6aWrapping w = p.wrapping(10);
		System.out.println(w.value());
	}
} // /:~
