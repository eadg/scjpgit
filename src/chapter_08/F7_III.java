/** Created on 2008-12-30 */
package chapter_08;

interface Cookable {
	public void cook();
}

class Food_F7 { // F7
	Cookable c = new Cookable() {
		public void cook() {
			System.out.println("anonymous cookable");
		}
	};
}
