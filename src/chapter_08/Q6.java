/** Created on 2008-12-21 */
package chapter_08;

class Q6 { // F2
	private String x = "Outer2";

	void doStuff() {
		final String z = "final local variable z";
		abstract class MyInner1 {
			private String u = "nonfinal local variable u";

			public void seeOuter() {
				System.out.println("Outer x is " + x);
				System.out.println("Local z is " + z);
				System.out.println("Local u is " + u);
			}
		}
		abstract class MyInner2 {
		}
	}

	public static void main(String[] arg) {

	}
}