/** Created on 2008-12-30 */
package chapter_08;

class Popcorn { // F1
	public void pop() {
		System.out.println("popcorn");
	}
}

class Food {
	Popcorn_F5 p = new Popcorn_F5() {
		public void pop() {
			System.out.println("anon popcorn");
		}
	};
}

public class F1_III {
	public static void main(String[] args) {
		Food f = new Food();
		f.p.pop();
	}
}
