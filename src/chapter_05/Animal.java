/** Created on 2008-12-06 */
package chapter_05;

abstract class Animal {
	public abstract void checkup();
}

class Dog extends Animal {
	public void checkup() {
		System.out.println("Dogcheck");
	}
}

class Cat extends Animal {
	public void checkup() {
		System.out.println("Catcheck");
	}
}

class Bird extends Animal {
	public void checkup() {
		System.out.println("Birdcheck");
	}
}
