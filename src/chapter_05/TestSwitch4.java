package chapter_05;

public class TestSwitch4 {

	public static void main(String[] args) {
		int x = 2; // F24
		switch (x) {
		case 2: {
			System.out.println("2");
			break;
		}
		default: {
			System.out.println("default");
			break;
		}
		case 3: {
			System.out.println("3");
			break;
		}
		case 4: {
			System.out.println("4");
			break;
		}
		} // OUT: 2
	}
}
