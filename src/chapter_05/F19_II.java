/** Created on 2009-03-27 */
package chapter_05;

public class F19_II {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int x;
		long x2; // F19
		Long[] La = { 4L, 5L, 6L };
		long[] la = { 7L, 8L, 9L };
		int[][] twoDee = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
		String[] sNums = { "one", "two", "three" };
		Animal[] animals = { new Dog(), new Cat() };
		// legal 'for' declarations
		for (long y : la)
			; // loop thru array of longs
		for (long lp : La)
			; // autoboxing Long objects into longs
		for (int[] n : twoDee) { // loop thru array of arrays
			for (int l1 : n) {
				System.out.println("petla 1 " + l1);
			}
		}
		for (int l2 : twoDee[2]) { // loop thru 3rd sub-array
			System.out.println("petla 2 " + l2);
		}
		for (String s : sNums)
			; // loop thru array of Strings
		for (Object o : sNums)
			; // set Object ref each String
		for (Animal a : animals) { // set Animal ref to each element
			System.out.print("petla 3 ");
			a.checkup();
		}
		// for(long x2 : la) {} //duplicate local variable x2
		// for (int x2 = 0; ;) {}
	}

}
