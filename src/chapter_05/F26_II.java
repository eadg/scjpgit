package chapter_05;

public class F26_II {

	public static void main(String[] args) {
		outer: // F26
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				System.out.println("Hello");
				continue outer;
			} // end of inner loop
			System.out.println("outer");
			// Never prints
		}
		System.out.println("Good-Bye");
	}
}
