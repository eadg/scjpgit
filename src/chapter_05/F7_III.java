package chapter_05;

public class F7_III {
	static void doStuff() {
		doMoreStuff();
	}

	static void doMoreStuff() {
		int x = 5 / 0; // Can't divide by zero!
		// ArithmeticException is thrown here
	}

	public static void main(String... strings) {
		doStuff();
	}
}
