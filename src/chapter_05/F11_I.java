package chapter_05;

public class F11_I {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int x = 3; // F11
		if (x == 1) {
			System.out.println("x equals 1");
		} else if (x == 2) {
			System.out.println("x equals 2");
		} else if (x == 3) {
			System.out.println("x equals 3");
		} else {
			System.out.println("No idea what x is");
		}
	}

}
