package chapter_05;

import java.io.*;

class Master {
	String doFileStuff() throws FileNotFoundException {
		return "a";
	}
}

class Q3 extends Master {
	public static void main(String[] args) {
		String s = null;
		try {
			s = new Q3().doFileStuff();
		} catch (Exception x) {
			s = "b";
		}
		System.out.println(s);
	}

	// String doFileStuff() { return "b"; } //OK
	// String doFileStuff() throwDs IOException ( return "b"; } //FALSE
	// String doFileStuff(int x) throws IOException { return "b"; } //OK, BUT
	// NOT IS USING doFileStuff()
	// String doFileStuff() throws FileNotFoundException { return "b"; }
	// String doFileStuff() throws NumberFormatException { return "b"; }
	String doFileStuff() throws NumberFormatException, FileNotFoundException {
		return "b";
	} // OK, BECAUSE RUNTIME EXCEPTION
		// String doFileStuff() throws EOFException, FileNotFoundException {
		// return
		// "b"; } //FALSE, BECAUSE EOFException
}
