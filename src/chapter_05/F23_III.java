package chapter_05;

class F23_III_MyException extends Exception {

	private static final long serialVersionUID = 1L;
}

public class F23_III extends Exception {

	private static final long serialVersionUID = 1L;

	void someMethod() throws F23_III_MyException {
		doStuff();
	}

	void doStuff() throws F23_III_MyException {
		try {
			throw new F23_III_MyException();
		} catch (F23_III_MyException me) {
			throw me;
		}
	}

	public static void main(String[] args) {
		F23_III f23 = new F23_III();
		try {
			f23.someMethod();
		} catch (F23_III_MyException mye) {
			mye.printStackTrace();
		}
	}
}
