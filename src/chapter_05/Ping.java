package chapter_05;

public class Ping extends Utils {
	public static void main(String[] args) throws Exception {
		Utils u = new Ping();
		System.out.print(u.getInt(args[0]));
		/*
		 * OR try { System.out.print(u.getInt(args[0])); } catch(Exception iae)
		 * {}
		 */
	}

	int getInt(String arg) {
		return Integer.parseInt(arg);
	}
}

class Utils {
	int getInt(String x) throws Exception {
		return 7;
	}
}
