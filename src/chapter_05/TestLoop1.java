package chapter_05;

public class TestLoop1 {

	public static void main(String[] args) {
		for (int i = 0; i < 4; i++) { // F21
			System.out.println("Inside loop" + i);
			if (i == 2) {
				continue;
			}
			System.out.println("Still inside loop;) " + i);
			// more loop code, that won't be reached when above if test is true
		}
	}
}
