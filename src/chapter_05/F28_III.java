package chapter_05;

import java.io.*;

class F28_III_Master {
	String doFileStuff() throws FileNotFoundException {
		return "a";
	}
}

public class F28_III extends F28_III_Master {
	public static void main(String[] args) {
		String s = null;
		try {
			s = new F28_III().doFileStuff();
		} catch (Exception x) {
			s = "b";
		}
		System.out.println(s);
	}

	String doFileStuff() throws NumberFormatException {
		return "b";
	}
}
