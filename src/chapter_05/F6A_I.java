package chapter_05;

public class F6A_I {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (true) { // F6A
			if (true) {
				System.out.println("Try again.");
				// Which if does this belong to?
			} else {
				System.out.println("Java master!");
			}
		}
	}

}
