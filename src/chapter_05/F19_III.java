package chapter_05;

import java.io.*; //F19

public class F19_III {
	public int myMethod1() throws EOFException {
		return myMethod2();
	}

	public int myMethod2() throws EOFException {
		// code could throw exception
		return 1;
	}

	public static void main(String[] args) {
		F19_III f19 = new F19_III();
		try {
			f19.myMethod1();
		} catch (EOFException e) {
		}
	}
}
