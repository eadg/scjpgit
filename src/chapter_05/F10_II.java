/** Created on 2009-03-27 */
package chapter_05;

public class F10_II {
	static boolean doStuff() { // F10
		for (int x = 0; x < 3; ++x) {
			System.out.println("in for loop");
			System.out.println("x " + x);
			return true;
		}
		return true;
	}

	public static void main(String... args) {
		doStuff();
	}
}
