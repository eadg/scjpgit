package chapter_05;

public class F24_III {
	static void badMethod() {
		doStuff();
	}

	static void doStuff() {
		throw new Error();
		// try { throw new Error(); }
		// catch(Error me) { throw me; }
	}

	public static void main(String[] args) {
		// Error e; //duplicate e
		try {
			badMethod();
		} catch (Error e) {
		} catch (Throwable t) {
		}
	}
}
