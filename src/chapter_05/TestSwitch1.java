package chapter_05;

public class TestSwitch1 {

	public static void main(String[] args) {
		final int a = 1;
		final int b = 3;
		// b = 2;
		int x = 0;
		switch (x) {
		case a: // ok
		case b:
		} // compiler error
	}
}
