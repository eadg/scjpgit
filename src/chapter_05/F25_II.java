/** Created on 2009-03-27 */
package chapter_05;

public class F25_II {

	public static void main(String[] args) {
		boolean isTrue = true; // F25
		outer: for (int i = 0; i < 5; i++) {
			while (isTrue) {
				System.out.println("Hello");
				break outer;
			}
			System.out.println("Outer loop.");
			// Won't print
		} // end of outer for loop
		System.out.println("Good-Bye");

	}

}
