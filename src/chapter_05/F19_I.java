package chapter_05;

enum Color {
	red, green, blue
}

public class F19_I {

	public static void main(String[] args) {
		Color c = Color.green;
		switch (c) { // F19
		case red:
			System.out.print("red ");
		case green:
			System.out.print("green ");
		case blue:
			System.out.print("blue ");
		default:
			System.out.println("done");
		}
	}

}
