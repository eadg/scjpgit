package chapter_05;

public class F13_I {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		final int a = 1;
		final int b;
		// final int b =2;
		b = 2;
		int x = 0; // F13
		switch (x) {
		case a: // ok
			// case b: // compiler error
		}
		System.out.println(b);
	}

}
