package chapter_05;

public class F27_III {
	public void doStuff() throws Exception { // F27
		try {
			throw new Exception();
		} catch (Exception ex) {
			throw ex;
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//
		F27_III foo = new F27_III();
		try {
			foo.doStuff();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
