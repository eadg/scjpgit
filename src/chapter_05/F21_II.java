/** Created on 2009-03-27 */
package chapter_05;

public class F21_II {
	public static void main(String... strings) {
		for (int i = 0; i < 4; i++) { // F21
			System.out.println("Inside loop" + i);
			if (i == 2) {
				continue;
			}
			System.out.println("Still inside loop;) " + i);
		}
	}
}
