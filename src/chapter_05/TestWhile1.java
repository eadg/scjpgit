package chapter_05;

public class TestWhile1 {

	public static void main(String[] args) {
		int x = 2;
		while (x == 2) {
			System.out.println(x);
			++x;
		}
		System.out.println("Outside While");
	}
}