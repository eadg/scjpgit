package chapter_05;

public class TestSwitch3 {

	public static void main(String[] args) {
		int x = 5;
		switch (x) {
		case 2:
		default: {
			System.out.println("default");
			break;
		}
		case 4:
		case 6:
		case 8:
		case 10: {
			System.out.println("x is " + x);
		}

		}
	}
}
