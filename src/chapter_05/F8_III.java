package chapter_05;

public class F8_III {
	static String reverse(String str) throws Exception {
		if (str.length() == 0) {
			throw new Exception();
		}
		String rev = "";
		for (int i = str.length() - 1; i >= 0; --i) {
			rev = rev + str.charAt(i);
		}
		System.out.println(rev);
		return rev;
	}

	public static void main(String[] args) {
		try {
			reverse("dupa");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
