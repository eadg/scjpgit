package chapter_06;

import java.util.regex.*;

public class F5a3_V {

	public static void main(String[] args) {
		Pattern p = Pattern.compile(".*xx");
		Matcher m = p.matcher("yyxxxyxx");
		boolean b = false;
		System.out.println("Pattern is " + m.pattern());
		while (b = m.find()) {
			System.out.println(m.start() + " " + m.group());
		}
	}
}
