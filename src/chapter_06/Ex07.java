/** Created on 2008-10-30 */
package chapter_06;

import java.io.*;

public class Ex07 {
	public static void main(String[] args) {
		File dir = new File("/tmp/dir3");
		dir.mkdir();
		File file = new File("/tmp/dir3", "file3");

		try {
			file.createNewFile();
		} catch (IOException ioe) {
			System.out.println(ioe.getStackTrace());
		}
	}

}
