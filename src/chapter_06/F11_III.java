package chapter_06;

import java.io.*;

class F11_A_III {
	public transient String s = "a jednak :)";
}

class F11_B_III extends F11_A_III implements Serializable {
	transient String s = "o qrde! :)";
}

public class F11_III {
	public static void main(String[] args) throws IOException,
			ClassNotFoundException {
		F11_A_III a = new F11_A_III();
		F11_B_III b = new F11_B_III();
		F11_B_III b2;
		FileOutputStream fs = new FileOutputStream("/tmp/testSer.ser");
		ObjectOutputStream os = new ObjectOutputStream(fs);
		os.writeObject(b); // 3
		// os.writeObject(a); //java.io.NotSerializableException
		os.close();
		FileInputStream fis = new FileInputStream("/tmp/testSer.ser");
		ObjectInputStream ois = new ObjectInputStream(fis);
		a = (F11_B_III) ois.readObject(); // 4
		ois.close();
		System.out.println(a.s);
	}
}
