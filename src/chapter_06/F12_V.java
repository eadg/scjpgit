package chapter_06;

import java.util.Scanner; //F12

public class F12_V {
	public static void main(String[] args) {
		boolean b2, b;
		int i;
		String s, hits = " ";
		Scanner s1 = new Scanner("1 true 34 hi");
		Scanner s2 = new Scanner("1 true trus 34 hi");
		while (s1.hasNext()) {
			s = s1.next();
			// hits += "s";
			System.out.println(s);
		}

		while (s2.hasNext()) {
			if (s2.hasNextInt()) {
				i = s2.nextInt();
				hits += "i";
			} else if (s2.hasNextBoolean()) {
				b2 = s2.nextBoolean();
				hits += "B";
			} else {
				s2.next();
				hits += "S";
			}
		}
		System.out.println("hits " + hits);
	}
}
