package chapter_06;

public class Q13 {
	public static void main(String[] args) {
		int x = 4;
		StringBuffer sb = new StringBuffer("..fedcba");
		sb.delete(3, 6);
		System.out.println(sb);
		sb.insert(3, "az");
		System.out.println(sb);
		if (sb.length() > 6)
			x = sb.indexOf("b");
		System.out.println(sb + "  " + x);
		sb.delete((x - 3), (x - 2));
		System.out.println(sb);
	}
}
