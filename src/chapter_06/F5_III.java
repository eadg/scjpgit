package chapter_06;

import java.io.*;

class Dog implements Serializable {

	private static final long serialVersionUID = 1L;

	private Collar theCollar;

	private int dogSize;

	public Dog(Collar collar, int size) {
		theCollar = collar;
		setDogSize(size);
	}

	public Collar getCollar() {
		return theCollar;
	}

	public int getDogSize() {
		return dogSize;
	}

	public void setDogSize(int dogSize) {
		this.dogSize = dogSize;
	}
}

class Collar implements Serializable {

	private static final long serialVersionUID = 1L;
	private int collarSize;

	public Collar(int size) {
		collarSize = size;
	}

	public int getCollarSize() {
		return collarSize;
	}
}

public class F5_III {
	public static void main(String[] args) {
		Collar c = new Collar(3);
		Dog d = new Dog(c, 5);
		System.out.println("before: collar size is "
				+ d.getCollar().getCollarSize());
		try {
			FileOutputStream fs = new FileOutputStream("/tmp/testSer.ser");
			ObjectOutputStream os = new ObjectOutputStream(fs);
			os.writeObject(d);
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			FileInputStream fis = new FileInputStream("/tmp/testSer.ser");
			ObjectInputStream ois = new ObjectInputStream(fis);
			d = (Dog) ois.readObject();
			ois.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("after: collar size is "
				+ d.getCollar().getCollarSize());
	}
}
