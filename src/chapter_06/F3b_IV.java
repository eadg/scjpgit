package chapter_06;

import java.util.*; //F3

class F3b_IV {
	public static void main(String[] args) {
		Date d1 = new Date();
		System.out.println("1st date " + d1.toString());
		Calendar c = Calendar.getInstance();
		c.setTime(d1); // #1
		System.out.println("Januar " + Calendar.JANUARY);
		System.out.println("SUNday " + Calendar.SUNDAY);
		System.out.println("First Day " + c.getFirstDayOfWeek());
		System.out.println("DAY_OF_WEEK_IN_MONTH "
				+ Calendar.DAY_OF_WEEK_IN_MONTH);
		System.out.println("DAY_OF_MONTH " + c.get(Calendar.DAY_OF_MONTH));

	}
}
