package chapter_06;

import java.text.*; //F5A
import java.util.*;

public class F5a_IV {
	public static void main(String[] args) {
		Date d1 = new Date(1000000000000L);
		DateFormat[] dfa = new DateFormat[6];
		dfa[0] = DateFormat.getInstance();
		dfa[1] = DateFormat.getDateInstance();
		dfa[2] = DateFormat.getDateInstance(DateFormat.SHORT);
		dfa[3] = DateFormat.getDateInstance(DateFormat.MEDIUM);
		dfa[4] = DateFormat.getDateInstance(DateFormat.LONG);
		dfa[5] = DateFormat.getDateInstance(DateFormat.FULL);
		for (int i = 0; i < dfa.length; i++)
			System.out.println("index " + i + "  :  " + dfa[i].format(d1));
	}
}
// OUT: 9/8/01 7:46 PM | Sep 8, 2001 | 9/8/01 | Sep 8, 2001 | September 8, 2001
// | Saturday, September 8, 2001

