package chapter_06;

import java.io.*;

public class F7_II {
	public static void main(String... strings) {
		try {
			File myDir = new File("/tmp/mydir");
			myDir.mkdir();
			File myFile = new File(myDir, "myFile.txt");
			myFile.createNewFile(); // F8
			PrintWriter prw = new PrintWriter(myFile);
			prw.println("jakiś string");
			prw.flush();
			prw.close();
		} catch (IOException ioe) {
			System.out.println(ioe);
			ioe.printStackTrace();
		}
	}
}
