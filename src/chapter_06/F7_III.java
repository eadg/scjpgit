package chapter_06;

import java.io.*; //F7

class Dog_F7_III implements Serializable {

	private static final long serialVersionUID = 1L;

	transient private Collar_F7_III theCollar;

	private int dogSize;

	public Dog_F7_III(Collar_F7_III collar, int size) {
		theCollar = collar;
		dogSize = size;
	}

	public Collar_F7_III getCollar() {
		return theCollar;
	}

	private void writeObject(ObjectOutputStream os) throws IOException {
		try {
			os.defaultWriteObject(); // 2
			os.writeInt(theCollar.getCollarSize()); // 3
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void readObject(ObjectInputStream is) throws IOException,
			ClassNotFoundException { // 4
		try {
			is.defaultReadObject(); // 5
			theCollar = new Collar_F7_III(is.readInt()); // 6
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

class Collar_F7_III {

	// private static final long serialVersionUID = 1L;
	private int collarSize;

	public Collar_F7_III(int size) {
		collarSize = size;
	}

	public int getCollarSize() {
		return collarSize;
	}
}

class F7_III {
	public static void main(String[] args) throws IOException {
		Dog_F7_III dog = new Dog_F7_III(new Collar_F7_III(5), 10);
		FileOutputStream fos = new FileOutputStream("/tmp/fos.ser");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(dog);
	}
}
