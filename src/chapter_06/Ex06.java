/** Created on 2008-10-29 */
package chapter_06;

import java.io.*;

class Keyboard {
}

public class Ex06 implements Serializable {
	private Keyboard k = new Keyboard();

	public static void main(String[] args) {
		Ex06 c = new Ex06();
		c.storeIt(c);
	}

	void storeIt(Ex06 c) {
		try {
			ObjectOutputStream os = new ObjectOutputStream(
					new FileOutputStream("myFile"));
			os.writeObject(c);
			os.close();
			System.out.println("done");
		} catch (Exception x) {
			System.out.println(x);
		}
	}
}
