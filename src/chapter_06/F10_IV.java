package chapter_06;

import java.text.*;

public class F10_IV {

	public static void main(String[] args) {
		float f1 = 123.45648f; // F10
		int i = 0;
		NumberFormat nf = NumberFormat.getInstance();
		System.out.println(i++ + ")" + nf.getMaximumFractionDigits() + " ");
		System.out.println(i++ + ")" + nf.format(f1) + "  ");
		nf.setMaximumFractionDigits(5);
		System.out.println(i++ + ")" + nf.format(f1) + "  ");
		try {
			nf.setParseIntegerOnly(false);
			nf.setMinimumFractionDigits(2);
			Number nb = nf.parse("1234,567f");
			System.out.println(i++ + ")" + nb.floatValue());
			nf.setParseIntegerOnly(true);
			System.out.println(i++ + ")" + nf.parse("1234,567f"));
		} catch (ParseException pe) {
			System.out.println("parse exc");
		}
	}
}