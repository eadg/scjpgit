package chapter_06;

import java.util.*;
import java.text.*;

public class F9_IV {

	public static void main(String[] args) {
		float f1 = 123.4567f;
		Locale locUS = new Locale("uk", "UK"); // France
		NumberFormat[] nfa = new NumberFormat[10];
		nfa[0] = NumberFormat.getNumberInstance();
		nfa[1] = NumberFormat.getNumberInstance(locUS);
		nfa[2] = NumberFormat.getCurrencyInstance();
		nfa[3] = NumberFormat.getCurrencyInstance(locUS);
		nfa[4] = NumberFormat.getInstance(locUS);
		nfa[5] = NumberFormat.getInstance();
		int i = 1;
		for (NumberFormat nf : nfa) {
			System.out.println(nfa.length - i + " " + nf.format(f1));
			i++;
		}
	}
}