/** Created on 2008-10-30 */
package chapter_06;

import java.text.*;

public class NumberFormatTest {
	public static void main(String[] s) {
		String myString = NumberFormat.getInstance().format(10.3434);
		System.out.println(myString);
		System.out.println(NumberFormat.getInstance()
				.getMaximumFractionDigits());
		System.out.println(NumberFormat.getInstance()
				.getMinimumFractionDigits());
		try {
			System.out.println(NumberFormat.getInstance().parse("3434.5454"));
		} catch (ParseException pe) {
		}
	}
}
