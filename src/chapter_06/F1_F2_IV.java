package chapter_06;

import java.util.*; //F1(2)

class F1_F2_IV {
	public static void main(String[] args) {
		Date d1 = new Date(1000000000000L); // a trillion!
		System.out.println("1st date " + d1.toString());
		d1.setTime(d1.getTime() + 3600000); // 1hour
		System.out.println("new time " + d1.toString());
	}
}
