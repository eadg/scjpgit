package chapter_06;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class F5_III_Read {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Dog d = null;
		try {
			FileInputStream fis = new FileInputStream("/tmp/testSer.ser");
			ObjectInputStream ois = new ObjectInputStream(fis);
			d = (Dog) ois.readObject();
			ois.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("after: collar size is "
				+ d.getCollar().getCollarSize());
	}

}
