package chapter_06;

import java.io.*; //F3

class Writer2b {
	public static void main(String[] args) {
		try {
			File existingDir = new File("existingDir"); // assign a dir
			System.out.println(existingDir.isDirectory());
			File existingDirFile = new File("/tmp/ch7drI.txt"); // assign a file
			System.out.println(existingDirFile.isFile());
			FileReader fr = new FileReader(existingDirFile);
			BufferedReader br = new BufferedReader(fr); // make a Reader
			String s;
			while ((s = br.readLine()) != null)
				// read data
				System.out.println(s);
			br.close();
		} catch (IOException e) {
		}
	}
} // OUT: 12 howdy "\n" folks
