package chapter_06;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class F7_III_Read {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Dog_F7_III d = null;
		try {
			FileInputStream fis = new FileInputStream("/tmp/fos.ser");
			ObjectInputStream ois = new ObjectInputStream(fis);
			d = (Dog_F7_III) ois.readObject();
			ois.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("after: collar size is "
				+ d.getCollar().getCollarSize());
	}

}
