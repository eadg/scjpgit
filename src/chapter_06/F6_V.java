package chapter_06;

import java.util.*; //F6

class F6_V {
	public static void main(String[] args) {
		System.out.print("input: ");
		System.out.flush();
		try {
			Scanner s = new Scanner(System.in);
			String token;
			do {
				token = s.findInLine(args[0]);
				System.out.println("found " + token);
			} while (token != null);
			System.out.println(args[0]);
		} catch (IllegalStateException e) {
			System.out.println("scan exc");
		}
	}
}
