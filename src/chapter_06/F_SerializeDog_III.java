package chapter_06;

import java.io.*;

public class F_SerializeDog_III {
	public static void main(String[] args) {
		Collar_SerializeDog_III c = new Collar_SerializeDog_III(3);
		Dog_SerializeDog_III d = new Dog_SerializeDog_III(c, 5);
		System.out.println("before: collar size is "
				+ d.getCollar().getCollar_SerializeDog_IIISize());
		try {
			FileOutputStream fs = new FileOutputStream("testSer.ser");
			ObjectOutputStream os = new ObjectOutputStream(fs);
			os.writeObject(d);
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			FileInputStream fis = new FileInputStream("testSer.ser");
			ObjectInputStream ois = new ObjectInputStream(fis);
			d = (Dog_SerializeDog_III) ois.readObject();
			ois.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("after: collar size is"
				+ d.getCollar().getCollar_SerializeDog_IIISize());
	}
}

class Dog_SerializeDog_III implements Serializable {
	private Collar_SerializeDog_III theCollar;

	private int dogSize;

	public Dog_SerializeDog_III(Collar_SerializeDog_III collar, int size) {
		theCollar = collar;
		dogSize = size;
	}

	public Collar_SerializeDog_III getCollar() {
		return theCollar;
	}
}

class Collar_SerializeDog_III implements Serializable {
	private int collarSize;

	public Collar_SerializeDog_III(int size) {
		collarSize = size;
	}

	public int getCollar_SerializeDog_IIISize() {
		return collarSize;
	}
}
