package chapter_06;

import java.util.*;
import java.text.*; //F5B

class F5d_IV {
	public static void main(String[] args) {
		Date d1 = new Date(1000000000000L);
		DateFormat[] dfa = new DateFormat[6];
		dfa[0] = DateFormat.getInstance();
		dfa[1] = DateFormat.getTimeInstance();
		dfa[2] = DateFormat.getTimeInstance(DateFormat.SHORT);
		dfa[3] = DateFormat.getTimeInstance(DateFormat.MEDIUM);
		dfa[4] = DateFormat.getTimeInstance(DateFormat.LONG);
		dfa[5] = DateFormat.getTimeInstance(DateFormat.FULL);
		for (int i = 0; i < dfa.length; i++)
			System.out.println("index " + i + "  :  " + dfa[i].format(d1));
		TimeZone t = dfa[0].getTimeZone();
		System.out.println("Time Zone " + t.getDisplayName());
		NumberFormat f = dfa[0].getNumberFormat();
		f.setMaximumFractionDigits(10);
		System.out.println(f.format(666.666));
		System.out.println("DATE_FIELD" + dfa[0].DATE_FIELD);
		Locale[] loc = DateFormat.getAvailableLocales();
		for (Locale locy : loc)
			System.out.println("table of Locale: " + locy);

	}
}
