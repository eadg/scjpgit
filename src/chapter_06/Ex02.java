/** Created on 2008-10-29 */
package chapter_06;

import java.io.*;

class Player {
	int i = 666;

	Player() {
		System.out.print("p");
	}
}

class Ex02 extends Player implements Serializable {
	Ex02() {
		System.out.print("c");
	}

	int i = 555;

	public static void main(String[] args) {
		Ex02 c1 = new Ex02();
		c1.i++;
		Player p2 = c1;

		try {
			FileOutputStream fos = new FileOutputStream("/tmp/play.txt");
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(c1);
			os.close();
			FileInputStream fis = new FileInputStream("/tmp/play.txt");
			ObjectInputStream is = new ObjectInputStream(fis);
			Ex02 c2 = (Ex02) is.readObject();
			is.close();
			System.out.println(c1.i + "  " + p2.i);
		} catch (Exception x) {
		}
	}
}
