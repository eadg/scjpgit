/** Created on 2008-10-29 */
package chapter_06;

import java.util.*;
import java.text.*;

class DateOne {
	public static void main(String[] args) {
		Date d = new Date(1123631685981L);
		// DateFormat df = new DateFormat();
		DateFormat df = DateFormat.getDateInstance();
		System.out.println(df.format(d));
	}
}
