/** Created on 2008-11-02 */
package chapter_06;

import java.util.*;

public class Fish_fromScannerApi {

	public static void main(String[] args) {
		String input = "1 fish 2 fish red fish blue fish";
		Scanner s = new Scanner(input).useDelimiter("\\s*fish\\s*");
		System.out.println(s.nextInt());
		System.out.println(s.nextInt());
		System.out.println(s.next());
		System.out.println(s.next());
		s.close();

	}

}
