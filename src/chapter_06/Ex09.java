/** Created on 2008-10-30 */
package chapter_06;

public class Ex09 {
	public static void main(String[] args) {
		System.out.format("%b", "d");
		System.out.println();
		// System.out.format("%c", "x");
		System.out.printf("%d", 123);
		System.out.println();
		// System.out.printf("%f", 123);
		// System.out.printf("%d", 123.45);
		System.out.printf("%f", 123.45);
		System.out.println();
		System.out.format("%s", new Long("123"));

	}

}
