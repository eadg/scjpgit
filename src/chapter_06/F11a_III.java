package chapter_06;

import java.io.*;

class F11a_A_III {
	public transient int s = 666;
}

class F11a_B_III extends F11a_A_III implements Serializable {

}

public class F11a_III {
	public static void main(String[] args) throws IOException,
			ClassNotFoundException {
		F11a_A_III a = new F11a_A_III();
		F11a_B_III b = new F11a_B_III();
		F11a_B_III b2;
		FileOutputStream fs = new FileOutputStream("/tmp/testSer.ser");
		ObjectOutputStream os = new ObjectOutputStream(fs);
		os.writeObject(b); // 3
		// os.writeObject(a); //java.io.NotSerializableException
		os.close();
		FileInputStream fis = new FileInputStream("/tmp/testSer.ser");
		ObjectInputStream ois = new ObjectInputStream(fis);
		b = (F11a_B_III) ois.readObject(); // 4
		ois.close();
		System.out.println(b.s);
	}
}
