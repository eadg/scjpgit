/** Created on 2008-11-02 */
package chapter_06;

import java.io.*;

public class F13_III {
	public static void main(String[] args) {
		SpecialSerial s1 = new SpecialSerial();
		s1.x = 999;
		s1.y = 777;
		try {
			ObjectOutputStream os = new ObjectOutputStream(
					new FileOutputStream("/tmp/myFile"));
			os.writeObject(s1);
			os.close();
			System.out.println("S1.z " + ++s1.z);
			ObjectInputStream is = new ObjectInputStream(new FileInputStream(
					"/tmp/myFile"));
			SpecialSerial s2 = (SpecialSerial) is.readObject();
			is.close();
			System.out.println("S2.y " + s2.y);
			System.out.println("S2.z " + s2.z);
			System.out.println("S2.t " + s2.st);
			System.out.println("S2.x " + s2.x);
		} catch (Exception x) {
			System.out.println("exc");
		}
	}
}

class SpecialSerial implements Serializable {
	transient int y = 7;

	transient String st = "tescior";

	static int z = 9;

	int x = 666;
}
