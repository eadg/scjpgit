package chapter_06;

import java.io.File;
import java.io.IOException;

public class F12_II {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		File delDir = new File("deldir");
		delDir.mkdir();
		File delFile1 = new File(delDir, "delFile1.txt");
		try {
			delFile1.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // F12
		File delFile2 = new File(delDir, "delFile2.txt");
		try {
			delFile2.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		delFile1.delete();
		System.out.println("delDir is " + delDir.delete());
		File newName = new File(delDir, "newName.txt");
		delFile2.renameTo(newName);
		File newDir = new File("newDir");
		delDir.renameTo(newDir);
		// OUT: delDir is false
	}

}
