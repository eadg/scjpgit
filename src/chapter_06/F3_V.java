package chapter_06;

import java.util.regex.*;

class F3_V {
	public static void main(String[] args) {
		Pattern p = Pattern.compile("\\d+");
		Matcher m = p.matcher("1 a12 234b");
		boolean b = false;
		while (b = m.find()) {
			System.out.println(m.start() + " " + m.group());
		}
	}
}
