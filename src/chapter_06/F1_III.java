package chapter_06;

import java.io.*; //F1

public class F1_III {
	public static void main(String[] args) {
		String name = "aldoniak";
		try {
			FileOutputStream fs = new FileOutputStream("/tmp/testSer.ser");
			ObjectOutputStream os = new ObjectOutputStream(fs);
			os.writeObject(name); // 3
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			FileInputStream fis = new FileInputStream("/tmp/testSer.ser");
			ObjectInputStream ois = new ObjectInputStream(fis);
			name = (String) ois.readObject(); // 4
			System.out.println(name);
			ois.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
