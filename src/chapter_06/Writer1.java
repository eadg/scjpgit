package chapter_06;

import java.io.*;

class Writer1 { // F2
	public static void main(String[] args) {
		try {
			boolean newFile = false;
			File file = new File("/tmp/fileWritel.txt");
			System.out.println(file.exists());
			newFile = file.createNewFile();
			System.out.println(newFile);
			System.out.println(file.exists());
		} catch (IOException e) {
		}
	}
} // OUT:false,true,true