package chapter_06;

import java.util.*; //F1

class F1_IV {
	public static void main(String[] args) {
		Calendar c = Calendar.getInstance();
		// assume c is October 8, 2001 //F4
		c.roll(Calendar.MONTH, 9); // notice year in output
		Date d4 = c.getTime();
		System.out.println("new date " + d4);
		// OUT:new date Fri Jul 08 19:46:40 MDT 2001

	}
}
