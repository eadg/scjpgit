package chapter_06;

import java.util.*;
import java.text.*;

public class F8_IV_B {
	static int i = 0;

	public static void main(String[] args) {
		Locale locIT = new Locale("it", "IT"); // Italy
		System.out.println(i++ + locIT.getDisplayCountry());
		System.out.println(i++ + locIT.getCountry());
		System.out.println(i++ + locIT.getDisplayCountry(locIT));
		System.out.println(i++ + locIT.getDisplayLanguage());
		System.out.println(i++ + locIT.getDisplayLanguage(Locale.GERMAN));
		System.out.println(i++ + locIT.getDisplayName(locIT));
		System.out.println(i++ + locIT.getISO3Country());
		System.out.println(i++ + locIT.getLanguage());

		/*
		 * for(Locale l : Locale.getAvailableLocales()) System.out.println(l);
		 */
	}
}
