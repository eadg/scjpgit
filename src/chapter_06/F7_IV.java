package chapter_06;

import java.util.*;
import java.text.*;

public class F7_IV {

	public static void main(String[] args) {
		Calendar c = Calendar.getInstance(); // F7
		c.set(2009, 4, 24); // December 14, 2010 month is 0-based
		Date d2 = c.getTime();
		Locale locIT = new Locale("it", "IT");
		Locale locPT = new Locale("pt");
		Locale locBR = new Locale("pt", "BR");
		Locale locIN = new Locale("hi", "IN");
		Locale locJA = new Locale("ja");
		Locale locPL = new Locale("pt", "PL");
		DateFormat dfUS = DateFormat.getInstance();
		System.out.println("US       " + dfUS.format(d2));
		DateFormat dfUSfull = DateFormat.getDateInstance(DateFormat.FULL);
		System.out.println("US full " + dfUSfull.format(d2));
		DateFormat dfIT = DateFormat.getDateInstance(DateFormat.FULL, locIT);
		System.out.println("Italy    " + dfIT.format(d2));
		DateFormat dfPT = DateFormat.getDateInstance(DateFormat.FULL, locPT);
		System.out.println("Portugal " + dfPT.format(d2));
		DateFormat dfBR = DateFormat.getDateInstance(DateFormat.FULL, locBR);
		System.out.println(locBR.getDisplayCountry() + dfBR.format(d2));
		DateFormat dfIN = DateFormat.getDateInstance(DateFormat.FULL, locIN);
		System.out.println(locIN.getDisplayCountry() + dfIN.format(d2));
		DateFormat dfJA = DateFormat.getDateInstance(DateFormat.FULL, locJA);
		System.out.println(locJA.getDisplayCountry() + dfJA.format(d2));
		DateFormat dfPL = DateFormat.getDateInstance(DateFormat.FULL, locPL);
		System.out.println(locPL.getDisplayCountry(locIT) + "\t"
				+ dfPL.format(d2));
	}
}
