package chapter_06;

import java.io.*;

;
public class F10_III implements Serializable {
	transient int i = 10;

	public static void main(String[] args) throws IOException,
			ClassNotFoundException {
		F10_III c = new F10_III();
		FileOutputStream fs = new FileOutputStream("/tmp/testSer.ser");
		ObjectOutputStream os = new ObjectOutputStream(fs);
		os.writeObject(c); // 3
		os.close();
		FileInputStream fis = new FileInputStream("/tmp/testSer.ser");
		ObjectInputStream ois = new ObjectInputStream(fis);
		c = (F10_III) ois.readObject(); // 4
		ois.close();
		System.out.println(c.i);
	}
}
