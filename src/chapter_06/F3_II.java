package chapter_06;

import java.io.*; //F3

class F3_II {
	public static void main(String[] args) {
		char[] in = new char[100]; // store input
		int size = 0;
		try {
			File file = new File("/tmp/fileWrite2.txt");
			FileWriter fw = new FileWriter(file);
			fw.write("howdy\nfolks\n");
			fw.flush();// flush before closing
			fw.close();// close file when done
			FileReader fr = new FileReader(file);
			size = fr.read(in); // read whole file!
			System.out.print(size + " ");
			// how many bytes read
			for (char c : in)
				// print array
				System.out.print(c);
			fr.close(); // always close
		} catch (IOException e) {
		}
	}
} // OUT: 12 howdy "\n" folks
