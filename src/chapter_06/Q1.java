package chapter_06;

import java.util.regex.*;

public class Q1 {
	public static void main(String[] args) {
		Pattern p = Pattern.compile("\\d*");
		Matcher m = p.matcher("ab34ef");
		boolean b = false;
		while (b = m.find()) {
			System.out.println(m.start() + "  " + m.group());
		}
	}

}
