package chapter_06;

import java.util.regex.*;

class F1_V {
	public static void main(String[] args) {
		Pattern p = Pattern.compile("ab");
		Matcher m = p.matcher("abababab");
		boolean b = false;
		while (m.find()) {
			System.out.println(m.start() + "\t" + m.group());
		}
	}
}
