package chapter_06;

import java.io.*;

public class F15_III {
	public static void main(String[] args) {
		F15_III_Dog d = new F15_III_Dog(35, "Fido");
		d.stringi = "zmiana";
		System.out.println("before: " + d.name + " " + d.weight);
		try {
			FileOutputStream fs = new FileOutputStream("testSer.ser");
			ObjectOutputStream os = new ObjectOutputStream(fs);
			os.writeObject(d);
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			FileInputStream fis = new FileInputStream("testSer.ser");
			ObjectInputStream ois = new ObjectInputStream(fis);
			d = (F15_III_Dog) ois.readObject();
			ois.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("after:  " + d.name + " " + d.weight + " "
				+ d.stringi);
	}
}

class F15_III_Dog extends F15_III_Animal implements Serializable {
	String name;

	F15_III_Dog(int w, String n) {
		weight = w; // inherited
		name = n; // not inherited
	}
}

class F15_III_Animal {
	int weight = 42;

	String stringi = "że co?";
}