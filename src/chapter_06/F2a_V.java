package chapter_06;

import java.util.regex.*;

class F2a_V {
	public static void main(String[] args) {
		Pattern p = Pattern.compile("[^a-cA-C]");
		Matcher m = p.matcher("cafeBABE");
		boolean b = false;
		while (b = m.find()) {
			System.out.println(m.start() + " " + m.group());
		}
	}
}
