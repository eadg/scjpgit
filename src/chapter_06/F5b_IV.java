package chapter_06;

import java.util.*;
import java.text.*; //F5B

class F5b_IV {
	public static void main(String[] args) {
		Date d1 = new Date(); // F5B
		System.out.println("d1 = " + d1.toString());
		// DateFormat df1=DateFormat.getDateInstance(DateFormat.FULL);
		DateFormat df1 = DateFormat.getInstance();
		DateFormat df2 = DateFormat.getDateInstance(DateFormat.MEDIUM);
		String s1 = df1.format(d1);
		String s2 = df2.format(d1);
		System.out.println("DateFormat #1 " + s1);
		System.out.println("DateFormat #2 " + s2);
		try {
			Date d2 = df1.parse(s1);
			Date d3 = df2.parse(s2);
			System.out.println("parsed = " + d2.toString());
			System.out.println("parsed = " + d3.toString());
		} catch (ParseException pe) {
			System.out.println("parse exc");
		}
		System.out.println("koniec");
	}
}
