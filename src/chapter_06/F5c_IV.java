package chapter_06;

import java.util.*;
import java.text.*; //F5B

class F5c_IV {
	public static void main(String[] args) {
		Date d1 = new Date(1000000000000L);
		DateFormat[] dfa = new DateFormat[9];
		dfa[0] = DateFormat.getDateTimeInstance();
		dfa[1] = DateFormat.getTimeInstance();
		dfa[2] = DateFormat.getTimeInstance(DateFormat.SHORT);
		dfa[3] = DateFormat.getTimeInstance(DateFormat.MEDIUM);
		dfa[4] = DateFormat.getTimeInstance(DateFormat.LONG);
		dfa[5] = DateFormat.getTimeInstance(DateFormat.FULL);
		dfa[6] = DateFormat.getInstance();
		dfa[7] = DateFormat.getDateInstance();
		for (int i = 0; i < dfa.length; i++)
			if (dfa[i] != null)
				System.out.println("index " + i + "  :  " + dfa[i].format(d1));

	}
}
