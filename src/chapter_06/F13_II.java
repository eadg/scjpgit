package chapter_06;

import java.io.File;

public class F13_II {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String[] files = new String[100]; // F13
		File search = new File("/tmp");
		files = search.list();
		for (String fn : files)
			System.out.println("found " + fn);
		System.out.println("ilość: " + files.length);
		// OUT: found dir1 found dir2 found dir3 found file1.txt found file2.txt
		// Your results will almost certainly vary : )
	}

}
