package chapter_06;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

class F8_III_Animal {
	int i = 5;
}

class F8_III_Dog extends F8_III_Animal implements Serializable {

	private static final long serialVersionUID = 1L;

}

public class F8_III {

	public static void main(String[] args) {
		F8_III_Dog f8 = new F8_III_Dog();
		try {
			FileOutputStream fs = new FileOutputStream("/tmp/F8_III_Dog.ser");
			ObjectOutputStream os = new ObjectOutputStream(fs);
			os.writeObject(f8);
			os.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
