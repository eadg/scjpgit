package chapter_09;

public class F7_IV extends Thread {
	Calculator c; // F7

	public F7_IV(Calculator calc) {
		c = calc;
	}

	public void run() {
		synchronized (c) {
			try {
				c.wait();
				System.out.println("Waiting for "
						+ Thread.currentThread().getName());
			} catch (InterruptedException e) {
			}
			System.out.println("Total is: " + c.total);
		}
	}

	public static void main(String[] args) {
		Calculator calculator = new Calculator();
		// Calculator calculator2 = new Calculator();
		new F7_IV(calculator).start();
		new F7_IV(calculator).start();
		new F7_IV(calculator).start();
		new F7_IV(calculator).start();
		new F7_IV(calculator).start();
		new F7_IV(calculator).start();
		new F7_IV(calculator).start();
		new F7_IV(calculator).start();
		new F7_IV(calculator).start();
		// new F7_IV(calculator2).start();
		calculator.start();
		// calculator2.start();

	}
}

class Calculator extends Thread {
	long total;

	public void run() {
		synchronized (this) {
			for (int i = 0; i < 9000000; i++) {
				total += i;
			}
			notifyAll();
			// notify();
		}
	}
}
