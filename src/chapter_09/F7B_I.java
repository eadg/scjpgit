/** Created on 2009-02-21 */
package chapter_09;

class NameRunnable_F7B extends Thread { // F7
	public void run() {
		System.out.println("NameRunnable_F7B running");
		System.out.println("Run by " + getName());
		try {
			System.out.println("sleeping");
			sleep(1000);
			System.out.println("This is the end...");
		} catch (InterruptedException e) {
		}
	}
}

public class F7B_I {
	public static void main(String[] args) {
		NameRunnable_F7B t = new NameRunnable_F7B();
		t.setName("Fred");
		t.start();
		t.setName("Fredka");
		System.out.println("Run by " + t.getName());
	}
}
