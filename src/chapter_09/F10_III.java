/** Created on 2009-01-25 */
package chapter_09;

class F10_III extends Thread {
	StringBuffer letter; // F10

	public F10_III(StringBuffer letter) {
		this.letter = letter;
	}

	public void run() {
		synchronized (letter) { // #1
			for (int i = 1; i <= 100; ++i)
				System.out.print(letter);
			System.out.println(Thread.currentThread().getName());
			try {
				sleep(1000);
			} catch (InterruptedException e) {
			}
			System.out.println();
			char temp = letter.charAt(0);
			++temp; // Increment letter in StringBuffer:
			letter.setCharAt(0, temp);
		} // #2
	}

	public static void main(String[] args) {
		StringBuffer sb = new StringBuffer("A");
		new F10_III(sb).start();
		new F10_III(sb).start();
		new F10_III(sb).start();
	}
}
