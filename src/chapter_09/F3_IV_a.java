/** Created on 2009-01-27 */
package chapter_09;

class F3_IV_a { // F3
	public static void main(String[] args) {
		ThreadB_a b = new ThreadB_a();
		b.start();
		synchronized (b) {
			// try {
			System.out.println("Waiting for b to complete");
			// b.wait();
			// } catch (InterruptedException e) {}
			System.out.println("Total is: " + b.total);
		}
		System.out.println("finish main() " + Thread.currentThread().getName());
	}
}

class ThreadB_a extends Thread {
	long total;

	public void run() {
		synchronized (this) {
			for (long i = 0; i < 100; i++) {
				total += i;
			}
			System.out.println(total);
			notify();
			System.out.println(Thread.currentThread().getName());
		}
	}
}