/** Created on 2009-02-21 */
package chapter_09;

class NameRunnable_F3_II implements Runnable { // F3
	public void run() {
		for (int x = 1; x < 4; x++) {
			System.out.println("Run by " + Thread.currentThread().getName());
			try {
				Thread.sleep(1);
			} catch (InterruptedException ex) {
			}
		}
	}
}

public class F3_II {
	public static void main(String[] args) {
		NameRunnable_F3_II nr = new NameRunnable_F3_II();
		Thread one = new Thread(nr);
		one.setName("Fred");
		Thread two = new Thread(nr);
		two.setName("Lucy");
		Thread three = new Thread(nr);
		three.setName("Ricky");
		one.start();
		two.start();
		three.start();
	}
}
