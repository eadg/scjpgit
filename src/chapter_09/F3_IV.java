/** Created on 2009-01-27 */
package chapter_09;

class F3_IV { // F3
	public static void main(String[] args) {
		ThreadB b = new ThreadB();
		b.start();
		synchronized (b) {
			try {
				System.out.println("Waiting for b to complete");
				b.wait();
			} catch (InterruptedException e) {
			}
			System.out.println("Total is: " + b.total);
		}
	}
}

class ThreadB extends Thread {
	long total;

	public void run() {
		synchronized (this) {
			for (long i = 0; i < 1000000000l; i++) {
				total += i;
			}
			notify();
		}
	}
}