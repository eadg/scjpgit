/** Created on 2009-02-22 */
package chapter_09;

import java.util.*; //F14

public class F14_III {
	private List names = new LinkedList();

	public synchronized void add(String name) {
		names.add(name);
	}

	public synchronized String removeFirst() {
		if (names.size() > 0)
			return (String) names.remove(0);
		else
			return null;
	}

	public static void main(String[] args) { // F13
		final F14_III n1 = new F14_III();
		n1.add("Ozymandias");
		class NameDropper extends Thread {
			public void run() {
				String name = n1.removeFirst();
				System.out.println(name);
			}
		}
		Thread t1 = new NameDropper();
		Thread t2 = new NameDropper();
		t1.start();
		t2.start();
	}
}
