package chapter_09;

public class Q13 {
	public static synchronized void main(String[] args)
			throws InterruptedException {
		Thread t = new Thread();
		t.start();
		System.out.print("X");
		synchronized (t) {
			t.wait(1000);
		}
		System.out.print("Y");
	}
}
