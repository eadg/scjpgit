package chapter_09;

class MyRunnable implements Runnable {
	public void run() { // F3
		System.out.println("Important job MyRunnable");
	}
}

public class F4_I {
	public static void main(String[] args) {
		MyRunnable r = new MyRunnable();
		Thread foo = new Thread(r);
		Thread bar = new Thread(r);
		Thread bat = new Thread(r);
		foo.start();
		bar.start();
		bat.start();
	}
}
