/** Created on 2009-02-21 */
package chapter_09;

class NameRunnable_F12 implements Runnable { // F12
	public void run() {
		for (int x = 1; x <= 2000; x++) {
			System.out.println("Run by " + Thread.currentThread().getName()
					+ ", x is " + x);
		}
	}
}

public class F12_I {
	public static void main(String[] args) {
		NameRunnable_F12 nr = new NameRunnable_F12();
		Thread one = new Thread(nr);
		Thread two = new Thread(nr);
		Thread three = new Thread(nr);
		one.setName("Fred");
		two.setName("Lucy");
		three.setName("Ricky");
		one.start();
		two.start();
		three.start();
	}
}
