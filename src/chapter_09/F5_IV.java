/** Created on 2009-03-22 */
package chapter_09;

class Operator_F5 extends Thread {
	public void run() { // F5
		while (true) {
			// Get shape from user
			synchronized (this) {
				// Do new machine steps from shape
				notify();
			}
		}
	}
}

class Machine_F5 extends Thread {
	Operator_F5 operator;

	// assume this gets initialized
	public void run() {
		while (true) {
			synchronized (operator) {
				try {
					operator.wait();
				} catch (InterruptedException ie) {
				}
				// Send machine steps to hardware
			}
		}
	}
}

public class F5_IV {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
