/** Created on 2009-03-24 */
package chapter_09;

public class Q2 extends Thread {
	private String name;

	public Q2(String name) {
		this.name = name;
	}

	public void run() {
		synchronized (this) {
			try {
				sleep(1000);
			} catch (InterruptedException e) {
			}
			write();
		}
	} // No guarantee - 2 independet objects

	// 2) OK //public synchronized void run() { write(); } //No guarantee - 2
	// independet objects
	// OK //public void run() { synchronized(Q2.class) { write(); } }
	// Ok //public void run () { synchronized (System.out) { write (); } }
	public void write() {
		System.out.print(name);
		System.out.print(name);
	}

	public static void main(String[] args) {
		new Q2("X").start();
		new Q2("Y").start();
	}
}
