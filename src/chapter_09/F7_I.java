/** Created on 2009-02-21 */
package chapter_09;

class NameRunnable_F7 implements Runnable { // F7
	public void run() {
		System.out.println("NameRunnable running");
		System.out.println("Run by " + Thread.currentThread().getName());
	}
}

public class F7_I {
	public static void main(String[] args) {
		NameRunnable_F7 nr = new NameRunnable_F7();
		Thread t = new Thread(nr);
		t.setName("Fred");
		t.start();
	}
}
