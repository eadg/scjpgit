/** Created on 2009-01-25 */
package chapter_09;

import java.util.*; //F12

public class F12_III {
	private List names = Collections.synchronizedList(new LinkedList());

	public void add(String name) {
		names.add(name);
	}

	public String removeFirst() {
		if (names.size() > 0)
			return (String) names.remove(0);
		else
			return null;
	}

	public static void main(String[] args) { // F13
		final F12_III n1 = new F12_III();
		n1.add("Ozymandias");
		class NameDropper extends Thread {
			public void run() {
				String name = n1.removeFirst();
				System.out.println(name);
			}
		}
		Thread t1 = new NameDropper();
		Thread t2 = new NameDropper();
		t1.start();
		t2.start();
	}
}
