package chapter_09;

public class Q7 {
	Integer a, b;

	public int read() {
		synchronized (a) {
			return a + b;
		}
	}

	public void get(Integer a, Integer b) {
		synchronized (a) {
			this.a = a;
			this.b = b;
		}
	}

	public static void main(String[] args) {

	}

}
