/** Created on 2009-02-21 */
package chapter_09;

class FooRunnable implements Runnable {
	public void run() { // F5
		for (int x = 1; x < 6; x++) {
			System.out.println("Runnable running: " + x);
		}
	}
}

public class F5_I {
	public static void main(String[] args) {
		FooRunnable r = new FooRunnable();
		Thread t = new Thread(r);
		t.start();
		// t.start();
	}
}
