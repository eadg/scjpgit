/** Created on 2009-03-25 */
package chapter_09;

class Q5 extends Thread {
	public static void main(String[] args) {
		Q5 t = new Q5();
		Thread x = new Thread(t);
		x.start();
	}

	public void run() {
		for (int i = 0; i < 3; i++) {
			System.out.print(i + "..");
		}
	}
}
