package chapter_09;

public class Q1 implements Runnable {
	public static void main(String[] args) {
		new Thread(new Q1("Wallace")).start();
		new Thread(new Q1("Gromit")).start();
	}

	private String name;

	public Q1(String name) {
		this.name = name;
	}

	public void run() {
		message(1);
		message(2);
	}

	private synchronized void message(int n) {
		System.out.print(name + "-" + n + " ");
	}
}

/*
 * Niezale�nie czy message(int) jest static
 */
