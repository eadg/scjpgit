package chapter_09;

public class F11_III { // F11
	private static int staticField;

	private int nonstaticField;

	public static synchronized int getStaticField() {
		return staticField;
	}

	public static synchronized void setStaticField(int staticField) {
		F11_III.staticField = staticField;
	}

	public synchronized int getNonstaticField() {
		return nonstaticField;
	}

	public synchronized void setNonstaticField(int nonstaticField) {
		this.nonstaticField = nonstaticField;
	}
}
