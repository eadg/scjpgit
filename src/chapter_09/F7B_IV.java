/** Created on 2009-03-22 */
package chapter_09;

class Reader_F7B extends Thread {
	Calculator_F7B c; // F7

	public Reader_F7B(Calculator_F7B calc) {
		c = calc;
	}

	public void run() {
		synchronized (c) {
			try {
				System.out.println("Waiting for calc");
				c.wait();
			} catch (InterruptedException e) {
			}
			System.out.println("Total is: " + c.total);
		}
	}
}

class Calculator_F7B extends Thread {
	int total;

	public void run() {
		try {
			sleep(1);
		} catch (InterruptedException e) {
		}
		synchronized (this) {
			for (int i = 0; i < 100000000; i++) {
				total += i;
			}
			notifyAll();
		}
	}
}

public class F7B_IV {
	public static void main(String[] args) {
		Calculator_F7B calculator = new Calculator_F7B();
		new Reader_F7B(calculator).start();
		new Reader_F7B(calculator).start();
		new Reader_F7B(calculator).start();
		// calculator.start();
	}
}
