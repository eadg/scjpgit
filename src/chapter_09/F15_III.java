/** Created on 2009-02-22 */
package chapter_09;

public class F15_III { // F15
	private static class Resource {
		public int value;
	}

	private Resource resourceA = new Resource();

	private Resource resourceB = new Resource();

	public int read() {
		synchronized (resourceA) { // MayDeadlockHere
			synchronized (resourceB) {
				return resourceB.value + resourceA.value;
			}
		}
	}

	public void write(int a, int b) {
		synchronized (resourceB) { // MayDeadlockHere
			synchronized (resourceA) {
				resourceA.value = a;
				resourceB.value = b;
			}
		}
	}
}