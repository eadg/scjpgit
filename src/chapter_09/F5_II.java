/** Created on 2009-02-21 */
package chapter_09;

class F5_II extends Thread { // F5
	public void run() {
		for (int i = 1; i <= 50; ++i) {
			System.out.println(i + "  " + getName());
			if (i % 10 == 0)
				System.out.println("Hahaha " + getName());
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
			}
		}
	}

	public static void main(String[] args) {
		F5_II t1 = new F5_II();
		t1.setName("Firstek");
		F5_II t2 = new F5_II();
		t2.setName("Sekundek");
		// t1.setPriority(1);
		t1.start();
		// t2.setPriority(10);
		t2.start();
		// Thread.currentThread().yield();
	}
}