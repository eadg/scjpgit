package whizlabs;

import java.util.*;

class Shape8 {
}

class Circle8 extends Shape8 {
}

class Rectangle8 extends Shape8 {
}

public class Generics8 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Vector<Shape8> pict = new Vector<Shape8>();
		pict.add(new Circle8());
		pict.add(new Rectangle8());
		Rectangle8 rect = (Rectangle8) pict.get(1);
	}

}
