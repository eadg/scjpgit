package whizlabs;

class A1 {
}

class B1 extends A1 {
}

class C1 extends B1 {
}

class D1 extends A1 {
}

public class Instances {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		B1 obj = new B1();
		if (obj instanceof B1 && !(obj instanceof C1)) {
			System.out.println("OK");
			if (obj instanceof A1) {
				System.out.println("OK");
				// if(obj instanceof D1) {System.out.println("OK");}
			}
		}

	}

}
