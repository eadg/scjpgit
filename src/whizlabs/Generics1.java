package whizlabs;

import java.util.*;

class Shape1 {
}

class Rectangle1 extends Shape1 {
}

public class Generics1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Vector v1 = new Vector<Shape1>();
		Vector<? extends Object> v2 = new Vector<Shape1>();
		Vector<? super Rectangle1> v3 = new Vector<Rectangle1>();
		// Vector<? super Shape1> v4 = new Vector<Rectangle1>();
	}

}
