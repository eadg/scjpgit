package whizlabs;

public class Autobox2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Float A = 9.0F;
		float a = (float) 9.0;
		System.out.println(A.equals(a));
		System.out.println(A >= a);
	}

}
