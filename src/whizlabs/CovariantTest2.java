package whizlabs;

class First {
	public Object method1() {
		return new String("Base");
	}
}

class Second extends First {
	public String method1() {
		return new String("Derived");
	}
}

public class CovariantTest2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		First o = new Second();
		String s = (String) o.method1();

	}

}
