package testKing;

class Q83a {
	public void foo() {
		System.out.println("Q83a foo");
	}
}

public class Q83 extends Q83a {
	// public void foo() throws Exception {
	public void foo() {
		System.out.println("Q83 foo");
		// throw new Exception();
	}

	public static void main(String[] args) {
		try {
			// new Q83().foo();
		} catch (Exception e) {
		}
	}
}
