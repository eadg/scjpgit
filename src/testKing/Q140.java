package testKing;

import java.io.*;

class Q140A {
	Q140A() {
		System.out.println("A");
	}
}

class Q140B extends Q140A implements Serializable {
	Q140B() {
		System.out.println("B");
	}
}

public class Q140 extends Q140B implements Serializable {
	int size = 40;

	public static void main(String[] args) {
		Q140 q1 = new Q140();
		try {
			FileOutputStream fos = new FileOutputStream("/tmp/testSer.ser");
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(q1);
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			FileInputStream fis = new FileInputStream("/tmp/testSer.ser");
			ObjectInputStream ois = new ObjectInputStream(fis);
			q1 = (Q140) ois.readObject();
			ois.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("after: Q140 size is " + q1.size);
	}
}
