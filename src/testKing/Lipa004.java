package testKing;

import java.util.*;

public class Lipa004 {
	public static List<?> metA(List<? super Double> lista1,
			List<? extends Number> lista2) {
		double d = 0;
		for (Number n : lista2) {
			d += n.doubleValue();
			lista1.add(d);
		}
		return lista1;
	}

	public static void main(String[] args) {
		ArrayList<Double> lis1 = new ArrayList<Double>();
		ArrayList<Long> lis2 = new ArrayList<Long>();
		lis2.add(1L);
		lis2.add(60L);
		lis2.add(77L);
		System.out.println(Lipa004.metA(lis1, lis2));

	}
}
