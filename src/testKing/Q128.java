package testKing;

class Q128Mother {
	public int number;

	protected Q128Mother(int number) {
		this.number = number;
	}
}

public class Q128 extends Q128Mother {
	private Q128(int number) {
		super(number);
	}

	public static void main(String[] args) {
		Q128 q = new Q128(420);
		System.out.println(q.number);
	}
}
