package testKing;

import java.util.*;

class Q86 {
	public static void main(String[] args) {
		Q86 q = new Q86();
		Collection<String> c = q.foo();
		for (String s : c) {
			System.out.println(s);
		}
	}

	Collection<String> foo() {
		Collection<String> q = new LinkedList<String>();
		q.add("B");
		q.add("A");
		q.add("C");
		System.out.println("foo: ");
		return q;
	}
}
