package testKing;

public class Q60 {
	public void testIfA() {
		if (testIfB("true"))
			System.out.println("true");
	}

	public Boolean testIfB(String str) {
		return Boolean.valueOf(str);
	}

	public static void main(String[] args) {
		Q60 q = new Q60();
		q.testIfA();
	}

}
