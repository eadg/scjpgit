package testKing;

public class Q113 implements Runnable {
	public void run() {
		System.out.println("Run");
		throw new RuntimeException("Problem");
		// System.out.println("EndMethod");
	}

	public void runTest() {
		System.out.println("RunTest");
		throw new RuntimeException("Problem");
	}

	public static void main(String[] args) {
		Q113 q = new Q113();
		Thread t = new Thread(new Q113());
		t.start();
		System.out.println("End");
		// throw new RuntimeException("Problem");
		q.runTest();
		q.runTest();
	}

}
