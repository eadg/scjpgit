package testKing;

public class Q158 {
	static final int[] tab = { 6, 6, 6 };

	static final int[] tab2;
	static {
		tab2 = new int[3];
		tab2[0] = 6;
		tab2[1] = 6;
	}

	// static final int[] tab3;
	// public static void set() { tab3 = new int[3]; }
	// wrong because final must be set when object is created (not after by use
	// method
	public static void main(String[] args) {

	}
}
