package testKing;

import java.util.*;

public class Q104 {
	static Object get(List list) {
		return list.get(0);
	}

	public static void main(String[] args) {
		Object o = Q104.get(new LinkedList());
		// Object o = Q104.get(new LinkedList<?>());
		// String s = Q104.get(new LinkedList<String>());
		String s = (String) Q104.get(new LinkedList<String>());
		Object o2 = Q104.get(new LinkedList<Object>());
	}

}
