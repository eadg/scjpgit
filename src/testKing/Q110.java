package testKing;

public class Q110<T> {
	private T element;

	public Q110(T element) {
		this.element = element;
	}

	public T getElement() {
		return element;
	}

	public static void main(String[] args) {
		Q110<String> qs = new Q110<String>("lala");
		Q110<Integer> qi = new Q110<Integer>(2);
		System.out.println(qs.getElement());
		System.out.println(qi.getElement());
	}

}
