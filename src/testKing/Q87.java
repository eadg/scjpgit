package testKing;

class Q87 {
	static void foo() throws Error {
		if (true)
			throw new AssertionError();
		System.out.println("foo");
	}

	public static void main(String[] args) {
		System.out.println("main_start");
		try {
			foo();
		} catch (Exception e) {
		}
		System.out.println("main_end");
	}
}
