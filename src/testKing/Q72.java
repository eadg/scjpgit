package testKing;

enum Q72 {
	ONE, TWO, THREE;
	public static void main(String[] args) {
		if (Q72.ONE == Q72.ONE) {
			System.out.println("true1");
		}
		if (Q72.ONE == Q72.TWO) {
			System.out.println("true2");
		}
		// if(Q72.ONE > Q72.ONE) {System.out.println("true3");}
		if ((Q72.ONE).equals(Q72.ONE)) {
			System.out.println("true4");
		}
		if ((Q72.ONE).equals(Q72.TWO)) {
			System.out.println("true5");
		}
		int i = (Q72.ONE).compareTo(Q72.ONE);
		System.out.println("true6: " + i);
		int i2 = (Q72.ONE).compareTo(Q72.TWO);
		System.out.println("true7: " + i2);

	}
}
