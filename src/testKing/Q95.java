package testKing;

import java.io.*;

public class Q95 {
	public static void main(String[] args) {
		File objectFile;
		try {
			Banana b = new Banana();
			Banana b2 = new Banana();
			objectFile = new File("/tmp/banana.ser");
			FileOutputStream fo = new FileOutputStream(objectFile);
			ObjectOutputStream ow = new ObjectOutputStream(fo);
			ow.writeObject(b);
			ow.close();
			FileInputStream fi = new FileInputStream(objectFile);
			ObjectInputStream or = new ObjectInputStream(fi);
			b2 = (Banana) or.readObject();
			or.close();
			System.out.printf("restore: " + b2.yellow + b2.juice + b2.good);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			objectFile = null; // correct scope in main()
			// fo=null;
			// fi=null;
			// ow=null;
			// or=null;
		}
	}
}

class Food implements Serializable {
	int good = 3;
}

class Fruit extends Food {
	int juice = 5;
}

class Banana extends Fruit {
	int yellow = 4;
}