package testKing;

class Q82 {
	public static void main(String[] args) {
		int i = 5;
		Q82 q = new Q82();
		q.foo(i);
		System.out.println("q: " + i);
	}

	void foo(int x) {
		System.out.println("foo: " + x++);
		System.out.println("foo: aha " + x);
		System.out.println("foo: " + ++x);
	}
}
