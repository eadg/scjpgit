package testKing;

interface Foo {
}

class Alpha implements Foo {
}

class Beta extends Alpha {
}

public class Q90 extends Beta {
	public static void main(String[] args) {
		Beta x = new Beta();
		Alpha a = x;
		// Q90 q = (Q90)x;
		Beta bq = new Q90();
		if (bq instanceof Foo) {
			System.out.println(true);
		}
		Q90 q = (Q90) bq;
		Foo f = (Alpha) x;
		Foo f2 = (Q90) x; // ClassCastException
		Beta b = (Beta) (Alpha) x;
	}

}
