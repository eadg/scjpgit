package testKing;

import java.util.*;

public class Lipa003 {
	public static Number metA(List<? extends Number> lista) {
		double d = 0;
		for (Number n : lista) {
			d += n.doubleValue();
		}
		return (int) d;
	}

	public static void main(String[] args) {

		ArrayList<Long> lis1 = new ArrayList<Long>();
		lis1.add(1L);
		lis1.add(60L);
		lis1.add(77L);
		System.out.println(Lipa003.metA(lis1));

	}
}
