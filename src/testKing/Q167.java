package testKing;

import java.util.*;

class Q167A {
}

class Q167B extends Q167A {
}

public class Q167 {
	public void go() {
		ArrayList<Q167B> aList = new ArrayList<Q167B>();
		takeList1(aList);
		// takeList2(aList);
		takeList3(aList);
		takeList4(aList);
		// takeList5(aList);

	}

	public void takeList1(ArrayList bList) {
	}

	public void takeList2(ArrayList<Q167A> bList) {
	}

	public void takeList3(ArrayList<? extends Q167A> bList) {
	}

	public void takeList4(ArrayList<?> bList) {
	}

	public void takeList5(ArrayList<Object> bList) {
	}

	public static void main(String[] args) {
	}

}
