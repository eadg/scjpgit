package testKing;

class Q85 {
	enum Dog {
		small, middle, big
	};

	public static void main(String[] args) {
		Dog d = Dog.middle;
		switch (d) {
		case middle:
			System.out.println(1);
		case small:
			System.out.println(2);
			break;
		case big:
			System.out.println(3);
		default:
			System.out.println(4);
		}
	}
}
