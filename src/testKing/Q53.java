/** Created on 2008-12-28 */
package testKing;

public class Q53 { // F2
	static void setBig() {
	}

	static class NestA {
		void go() {
			System.out.println("hi A");
		}
	}

	static class NestB extends NestA {
		void go() {
			System.out.println("hi B");
		}
	}

	public static void main(String[] args) {
		NestA n1 = new NestA();
		NestB n2 = new NestB();
		n1 = n2;
		NestA n3 = new NestB();
		n3.go();
	}
}

// TestKing has not right !