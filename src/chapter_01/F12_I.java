package chapter_01;

public class F12_I {
	public int getRecord(int fileNumber, final int recNumber) {
		// recNumber+=3;
		return recNumber + 3;
	} // F12

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		F12_I f = new F12_I();
		int i = f.getRecord(1, 2);
		System.out.println(i);
	}

}
