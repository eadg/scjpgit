package chapter_01;

class F26_CoffeeSize_2 {
	// OK. Other implent.
	public static final F26_CoffeeSize_2 BIG = new F26_CoffeeSize_2("BIG", 0);

	public static final F26_CoffeeSize_2 HUGE = new F26_CoffeeSize_2("HUGE", 1);

	public static final F26_CoffeeSize_2 OWHELMING = new F26_CoffeeSize_2(
			"OWHELMING", 2);

	public F26_CoffeeSize_2(String enumName, int index) {
	}

	public static void main(String[] args) {
		System.out.println(F26_CoffeeSize_2.BIG);
	}
}
// F26
