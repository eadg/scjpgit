package chapter_01;

class F26_TestCoffee {
	F26_CoffeeSize c;

	public static void main(String... arg) {
		F26_TestCoffee tc = new F26_TestCoffee();
		System.out.println(tc.c.BIG);
		System.out.println(tc.c.BIG.getOunces());
		System.out.println(tc.c.BIG.getLidCode());
		System.out.println(tc.c.OVERWHELMING.getLidCode());
		for (F26_CoffeeSize cs : F26_CoffeeSize.values())
			System.out.println(cs.name() + " getLidCode: " + cs.getLidCode()
					+ " " + "getOunces: " + cs.getOunces());

	}
}
