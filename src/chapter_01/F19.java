package chapter_01;

class F19 { // F19
	int count = 9; // Declare instance variable named count

	static int static_count = 11;

	public void logIn() {
		int count = 10; // Declare local variable named count
		System.out.println("local variable count is " + count);
	}

	public void count() {
		System.out.println("instance variable count is " + count);
	}

	public static void show_static_count() {
		System.out.println("instance variable count is " + static_count);
	}

	public static void main(String[] args) {
		new F19().logIn();
		new F19().count();
		F19.show_static_count();
	}
}