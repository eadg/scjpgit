package chapter_01;

enum F23b {
	// ; //GREAT :)
	BIG, HUGE, OVERWHELMING, SMALL("String"), SMALLI(2),

	;
	F23b() {
	}

	F23b(String s) {
		desc = s;
	}

	F23b(int i) {
	}

	String desc;

	public static void main(String[] args) {
		F23b f23b = F23b.BIG;
		System.out.println("works");
		System.out.println(f23b);
		System.out.println(F23b.values());

	}
}
