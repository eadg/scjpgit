package chapter_01;

class Coffee {
	CoffeeClass size;
}

public class F23 {
	public static void main(String[] args) {
		Coffee drink = new Coffee();
		drink.size = CoffeeClass.BIG; // enum outside class
		System.out.println(CoffeeClass.BIG.ordinal());
		System.out.println(CoffeeClass.BIG.name());
		System.out.println(CoffeeClass.valueOf("BIG"));
		System.out.println(CoffeeClass.BIG.getDeclaringClass());
	}
}
