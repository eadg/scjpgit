package chapter_01;

public class Q2 {
	public abstract class Canine1 {
		public abstract void speak();
	}

	public abstract class Canine2 {
		public void speak() {
		}
	}

	abstract public class Canine3 {
		public abstract void speak();
	}
	// public class Canine abstract{ public abstract void speak(); }
}