package chapter_01;

enum F24_CoffeeSize { // F24
	BIG(8), HUGE(10), OVERWHELMING(16);
	F24_CoffeeSize(int ounces) {
		this.ounces = ounces;
	}

	int ounces;

	public int getOunces() {
		return ounces;
	}
}