package chapter_01;

public class F25_CoffeeDrink { // F25
	F24_CoffeeSize size;

	public static void main(String[] args) {
		F25_CoffeeDrink drink1 = new F25_CoffeeDrink();
		drink1.size = F24_CoffeeSize.HUGE;
		F25_CoffeeDrink drink2 = new F25_CoffeeDrink();
		drink2.size = F24_CoffeeSize.OVERWHELMING;
		System.out.println(drink1.size.getOunces());
		System.out.println(drink2.size.getOunces());
	}
}
