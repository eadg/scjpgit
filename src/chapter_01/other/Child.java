package chapter_01.other;

import chapter_01.Parent;

class Child extends Parent {
	int x = 77;

	public void testIt() {
		System.out.println("x is " + x);
	}

	public static void main(String[] arg) {
		Child ch = new Child();
		ch.testIt();
	}
}
