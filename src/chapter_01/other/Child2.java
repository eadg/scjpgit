package chapter_01.other;

import chapter_01.Parent;

public class Child2 extends Parent {
	// int x = 2;
	public void testIt() {
		System.out.println("x is " + x); // No problem; Child inherits x
		Parent p = new Parent(); // No problem; Child inherits x
		// System.out.println("x in parent is" + p.x); // Compiler error!
		Child2 c = new Child2();
		int a = c.x;
		System.out.println(a);
	}

	public static void main(String arg[]) {
		Child2 ch2 = new Child2();
		ch2.testIt();
	}
}
