//: enumerated/TrafficLight.java
package chapter_01; /* Added by Eclipse.py */

// Enums in switch statements.
// Define an enum type:

public class F23d {
	CoffeeClass cc = CoffeeClass.BIG;

	void checkSize(CoffeeClass size) {
		switch (size) {
		case OVERKILL:
			System.out.println("To jest " + size);
			break;
		case HUGE:
			System.out.println("To jest " + size);
			break;
		case BIG:
			System.out.println("To jest " + size);
			break;

		}
	}

	public static void main(String[] args) {

		for (CoffeeClass s : CoffeeClass.values()) {
			System.out.println(s + " " + s.ordinal() + " " + s.name());
		}
		System.out.println("case....");
		F23d f23d = new F23d();
		f23d.checkSize(f23d.cc);
	}
}
