package calculator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegEx {
		
	
	double oblicz(String s) {
		
		Pattern pattern = Pattern.compile("(\\d+)([\\+|\\-|\\*]+)(\\d+)");
		Matcher matcher = pattern.matcher(s);
		double wynik = 0;
		while (matcher.find()) {
			switch(matcher.group(2).charAt(0)) {
				
				case '*':  {
					wynik = Integer.parseInt(matcher.group(1)) * Integer.parseInt(matcher.group(3));
					break;
					}
				case '+':  {
					wynik = Integer.parseInt(matcher.group(1)) + Integer.parseInt(matcher.group(3));
					break;
					}
				case '-':  {
					wynik = Integer.parseInt(matcher.group(1)) - Integer.parseInt(matcher.group(3));
					break;
					}
				case '/':  {
					wynik = Integer.parseInt(matcher.group(1)) / Integer.parseInt(matcher.group(3));
					break;
					}
				
			}
		}
		return wynik;
	}
}
