package calculator;

public class Main {

	Main() {
		System.out.println("Witaj w kalkulatorze!");
	}

	void showMenu() {
		System.out.println("Menu:");
		System.out.println("\tDodawanie - wciśnij +");
		System.out.println("\tMnożenie - wciśnij *");
		System.out.println("\tOdejmowanie - wciśnij -");
		System.out.println("\tDzielenie - wciśnij /");
		System.out.println("\tZakończenie kalkulatora - wciśnij pause");
	}

	public static void main(String[] args) {
		
		Operation o = new Operation();
		
		if (args.length == 0) {
			Main m = new Main();
			boolean flag = true;
			while (flag) {

				m.showMenu();
				int a = o.readCharInput();
				switch (a) {

				case 32: {
					System.out.println("do zobaczenia");
					flag = false;
					break;
				}
				case 43: {
					int ile = o.dodaj();
					System.out.println(ile);
					break;
				}
				case 42: {
					int ile = o.mnoz();
					System.out.println(ile);
					break;
				}
				case 45: {
					int ile = o.odejmij();
					System.out.println(ile);
					break;
				}

				}

			}
		}
		
		else {
				String s = o.arrayToString(args);
				RegEx re = new RegEx();
				System.out.println(re.oblicz(s));

		}

	}

}
