package calculator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Operation {

	char readCharInput() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		char i = 0;
		try {
			i = (char)br.read();
		} catch (IOException e) {
			System.out.println(e.getStackTrace());
		}
		return i;
	}
	
	int readStringInput() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int i = 0;
		try {
			i = Integer.parseInt(br.readLine());
		} catch (Exception e) {
			System.out.println("To nie jest liczba. Proszę próbować dalej.");
		}
		return i;		
	}
	
	int dodaj() {
		int i1, i2 = 0;
		System.out.println("Podaj pierwszą liczbę");
		i1 = readStringInput();
		System.out.println("Podaj drugą liczbę");
		i2 = readStringInput();
		return i1+i2;
	}
	
	int mnoz() {
		int i1, i2 = 0;
		System.out.println("Podaj pierwszą liczbę");
		i1 = readStringInput();
		System.out.println("Podaj drugą liczbę");
		i2 = readStringInput();
		return i1*i2;
	}	
	
	int odejmij() {
		int i1, i2 = 0;
		System.out.println("Podaj pierwszą liczbę");
		i1 = readStringInput();
		System.out.println("Podaj drugą liczbę");
		i2 = readStringInput();
		return i1-i2;
	}	
	
	String arrayToString(String[] arg) {
		StringBuilder sb= new StringBuilder();
		for(String s : arg) {
			sb.append(s.trim());
		}
		return sb.toString();
	}
}
